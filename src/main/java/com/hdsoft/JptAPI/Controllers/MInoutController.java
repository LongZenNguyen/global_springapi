package com.hdsoft.JptAPI.Controllers;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hdsoft.JptAPI.Models.MInout;
import com.hdsoft.JptAPI.Models.MInoutLine;
import com.hdsoft.JptAPI.Repositories.MInoutLineRepository;
import com.hdsoft.JptAPI.Repositories.MInoutRepository;
import com.hdsoft.JptAPI.Repositories.OrderRepository;

@RestController
@RequestMapping("/api/v1/minout")
public class MInoutController {
	
	@Autowired
	private MInoutRepository minoutRepository;
	
	@Autowired
	private OrderRepository orderRepo;
	
	@Autowired
	private MInoutLineRepository minoutlineRepo;
	
	@GetMapping
	public List<MInout> listAll() {
		return minoutRepository.findAll();
	}
	
	@GetMapping
	@RequestMapping("/{documentNo}")
	public MInout findByDocumentNo(@PathVariable String documentNo) {
		int size = minoutRepository.findByDocumentNoStartingWithOrderByDocumentNo(documentNo).size();
		return minoutRepository.findByDocumentNoStartingWithOrderByDocumentNo(documentNo).get(size - 1);
	}
	
	@GetMapping
	@RequestMapping("/orderid/{orderId}")
	public List<MInout> findByOrderId(@PathVariable long orderId) {
		return minoutRepository.findByOrderId(orderId);
	}
	
	@GetMapping
	@RequestMapping("/demo")
	public List<MInout> findAllDemo() {
		List<MInout> result = new ArrayList<>();
		List<MInout> listInout = minoutRepository.findByClientIdAndDoctypeIDAndDocStatus(1000000, 1000011, "CO");
		for (MInout inout : listInout) {
			Double qtyOrder = 0.0;
			Double qtyMovement = 0.0;
			List<MInoutLine> listInoutline = minoutlineRepo.findByMaterialID(inout.getMaterialID());
			for (MInoutLine inoutLine : listInoutline) {
				qtyOrder = qtyOrder + inoutLine.getQuantity();
				qtyMovement = qtyMovement + inoutLine.getQtyCheck();
			}
			if (qtyOrder - qtyMovement != 0.0) {
				inout.setNgayDatHang((Date) orderRepo.getOne(inout.getOrderId()).getNgayDatHang());
				result.add(inout);
			}
		}
		return result;
	}
	
}
