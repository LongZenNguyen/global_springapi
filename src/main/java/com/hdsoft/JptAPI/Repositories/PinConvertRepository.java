package com.hdsoft.JptAPI.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hdsoft.JptAPI.Models.PinConvert;

public interface PinConvertRepository extends JpaRepository<PinConvert, Integer>{

}
