package com.hdsoft.JptAPI.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hdsoft.JptAPI.Models.DoctypeTarget;

public interface DoctypeTargetRepository extends JpaRepository<DoctypeTarget, Long> {

}
