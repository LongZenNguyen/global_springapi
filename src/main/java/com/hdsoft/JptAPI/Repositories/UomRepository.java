package com.hdsoft.JptAPI.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hdsoft.JptAPI.Models.Uom;

public interface UomRepository extends JpaRepository<Uom, Long>{

}
