package com.hdsoft.JptAPI.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.hdsoft.JptAPI.Models.Orderline;

public interface OrderlineRepository extends JpaRepository<Orderline, Long>,CrudRepository<Orderline, Long>{
	
	public List<Orderline> findByOrderID(long orderID);
	public List<Orderline> findByOrderIDAndSanPham(long orderID, long sanPham);
	public List<Orderline> findAllByOrderID(long orderID);
	
	public List<Orderline> findByInvoicenoAndLotAndOrderID(String invoiceno,String lot,long orderID);
	public List<Orderline> findByInvoicenoAndSanPhamAndOrderID(String invoiceno,long sanPham,long orderID);

	//HDS App Base
	public List<Orderline> findByOrderIDAndSoLuongGreaterThan(long orderID,long soLuong);
//	public Orderline findByOrderIDAndSanPham(long orderID,long sanPham);
//	@Query(value="SELECT c1 FROM c_orderline c1 WHERE c_order_id = :c_order_id and m_product_id=:m_product_id")
//	public List<Orderline> AllDetailOrderLine(@Param("c_order_id") long c_order_id,@Param("m_product_id") long m_product_id);
//	
//	public List<Orderline> findByOrderIDAndSanPham(long orderID,long sanPham);
	@Query(value ="select c.palletno from C_Orderline c where c.c_orderline_id=?1",nativeQuery = true)
	public long getTotalPriceInPalletNo(long c_orderline_id);
}
