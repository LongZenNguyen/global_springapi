 package com.hdsoft.JptAPI.Models;

public class HDSerialProductMovement {

	
	long productId;
	double qtyMovement;

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public double getQtyMovement() {
		return qtyMovement;
	}

	public void setQtyMovement(double qtyMovement) {
		this.qtyMovement = qtyMovement;
	}

}
