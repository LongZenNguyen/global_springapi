 package com.hdsoft.JptAPI.Models;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MaterialReceipt {

	private long orderlineId;

	private long productId;

	private String productName;

	private int unitsperpack;

	private Long qtyOrder;

	private Long qtyCurrent;

	private Long qtyCheck;
	

	public MaterialReceipt() {

	}

	public MaterialReceipt(long orderlineId, long productId, String productName, int unitsperpack, Long qtyOrder,
			Long qtyCurrent, Long qtyCheck) {
		super();
		this.orderlineId = orderlineId;
		this.productId = productId;
		this.productName = productName;
		this.unitsperpack = unitsperpack;
		this.qtyOrder = qtyOrder;
		this.qtyCurrent = qtyCurrent;
		this.qtyCheck = qtyCheck;
	}

	public long getOrderlineId() {
		return orderlineId;
	}

	public void setOrderlineId(long orderlineId) {
		this.orderlineId = orderlineId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public Long getQtyOrder() {
		return qtyOrder;
	}

	public void setQtyOrder(Long qtyOrder) {
		this.qtyOrder = qtyOrder;
	}

	public Long getQtyCurrent() {
		return qtyCurrent;
	}

	public void setQtyCurrent(Long qtyCurrent) {
		this.qtyCurrent = qtyCurrent;
	}

	public Long getQtyCheck() {
		return qtyCheck;
	}

	public void setQtyCheck(Long qtyCheck) {
		this.qtyCheck = qtyCheck;
	}

	public int getUnitsperpack() {
		return unitsperpack;
	}

	public void setUnitsperpack(int unitsperpack) {
		this.unitsperpack = unitsperpack;
	}

}
