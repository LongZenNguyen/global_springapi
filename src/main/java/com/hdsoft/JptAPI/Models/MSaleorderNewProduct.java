 package com.hdsoft.JptAPI.Models;

public class MSaleorderNewProduct {

	private long productId;

	private long asiId;

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public long getAsiId() {
		return asiId;
	}

	public void setAsiId(long asiId) {
		this.asiId = asiId;
	}

}
