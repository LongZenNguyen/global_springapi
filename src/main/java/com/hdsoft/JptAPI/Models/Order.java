package com.hdsoft.JptAPI.Models;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "c_order")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Order {

	@Id
	@Column(name = "c_order_id")
	private Long id;

	@Column(name = "documentno")
	private String soChungTu;

	@Column(name = "description")
	private String dienGiai;

	@Column(name = "c_doctypetarget_id")
	private Long loaiChungTu;

	@Column(name = "dateordered")
	private Date ngayDatHang;

	@Column(name = "datepromised")
	private Date ngayDuKienGiaoHang;

	@Column(name = "c_bpartner_id")
	private Integer doiTac;

	@Column(name = "m_warehouse_id")
	private Long khoHang;

	@Column(name = "docstatus")
	private String docStatus;

	@Column(name = "c_doctype_id")
	private int doctypeId;
	private String docaction;
	@Column(name = "processed")
	private String processed;

	private String isvanning;

	private String isscanning;

	private String c_order_uu;
	@Column(name = "isback")
	private String isback;

	private int ad_client_id;
	@Column(name="ad_org_id")
	private long adorgid;
	private long createdby;
	private long updatedby;
	private Timestamp created;
	private Timestamp updated;
	private Timestamp dateacct;
	private long c_bpartner_location_id;
	private long c_currency_id;
	private String paymentrule;
	private long c_paymentterm_id;
	private String invoicerule;
	private String deliveryrule;
	private String freightcostrule;
	private String deliveryviarule;
	private String priorityrule;
	private long m_pricelist_id;

	public long getM_pricelist_id() {
		return m_pricelist_id;
	}

	public void setM_pricelist_id(long m_pricelist_id) {
		this.m_pricelist_id = m_pricelist_id;
	}

	public String getPriorityrule() {
		return priorityrule;
	}

	public void setPriorityrule(String priorityrule) {
		this.priorityrule = priorityrule;
	}

	public String getDeliveryviarule() {
		return deliveryviarule;
	}

	public void setDeliveryviarule(String deliveryviarule) {
		this.deliveryviarule = deliveryviarule;
	}

	public String getFreightcostrule() {
		return freightcostrule;
	}

	public void setFreightcostrule(String freightcostrule) {
		this.freightcostrule = freightcostrule;
	}

	public String getDeliveryrule() {
		return deliveryrule;
	}

	public void setDeliveryrule(String deliveryrule) {
		this.deliveryrule = deliveryrule;
	}

	public String getInvoicerule() {
		return invoicerule;
	}

	public void setInvoicerule(String invoicerule) {
		this.invoicerule = invoicerule;
	}

	public long getC_paymentterm_id() {
		return c_paymentterm_id;
	}

	public void setC_paymentterm_id(long c_paymentterm_id) {
		this.c_paymentterm_id = c_paymentterm_id;
	}

	public String getPaymentrule() {
		return paymentrule;
	}

	public void setPaymentrule(String paymentrule) {
		this.paymentrule = paymentrule;
	}

	public long getC_currency_id() {
		return c_currency_id;
	}

	public void setC_currency_id(long c_currency_id) {
		this.c_currency_id = c_currency_id;
	}

	public long getC_bpartner_location_id() {
		return c_bpartner_location_id;
	}

	public void setC_bpartner_location_id(long c_bpartner_location_id) {
		this.c_bpartner_location_id = c_bpartner_location_id;
	}

	public Timestamp getDateacct() {
		return dateacct;
	}

	public void setDateacct(Timestamp dateacct) {
		this.dateacct = dateacct;
	}

	public String getDocaction() {
		return docaction;
	}

	public void setDocaction(String docaction) {
		this.docaction = docaction;
	}

	public long getCreatedby() {
		return createdby;
	}

	public void setCreatedby(long createdby) {
		this.createdby = createdby;
	}

	public long getUpdatedby() {
		return updatedby;
	}

	public void setUpdatedby(long updatedby) {
		this.updatedby = updatedby;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

	public String getC_order_uu() {
		return c_order_uu;
	}

	public void setC_order_uu(String c_order_uu) {
		this.c_order_uu = c_order_uu;
	}

	public Order() { 
		super();
	}

	public Order(Long id, String soChungTu, String dienGiai, Long loaiChungTu, Date ngayDatHang,
			Date ngayDuKienGiaoHang, Integer doiTac, Long khoHang, String docStatus, int doctypeId, String processed,
			String isvanning, String isscanning, String isback, int ad_client_id, long ad_org_id) {
		super();
		this.id = id;
		this.soChungTu = soChungTu;
		this.dienGiai = dienGiai;
		this.loaiChungTu = loaiChungTu;
		this.ngayDatHang = ngayDatHang;
		this.ngayDuKienGiaoHang = ngayDuKienGiaoHang;
		this.doiTac = doiTac;
		this.khoHang = khoHang;
		this.docStatus = docStatus;
		this.doctypeId = doctypeId;
		this.processed = processed;
		this.isvanning = isvanning;
		this.isscanning = isscanning;
		this.isback = isback;
		this.ad_client_id = ad_client_id;
		this.adorgid = ad_org_id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSoChungTu() {
		return soChungTu;
	}

	public void setSoChungTu(String soChungTu) {
		this.soChungTu = soChungTu;
	}

	public String getDienGiai() {
		return dienGiai;
	}

	public void setDienGiai(String dienGiai) {
		this.dienGiai = dienGiai;
	}

	public Long getLoaiChungTu() {
		return loaiChungTu;
	}

	public void setLoaiChungTu(Long loaiChungTu) {
		this.loaiChungTu = loaiChungTu;
	}

	public Date getNgayDatHang() {
		return ngayDatHang;
	}

	public void setNgayDatHang(Date ngayDatHang) {
		this.ngayDatHang = ngayDatHang;
	}

	public Date getNgayDuKienGiaoHang() {
		return ngayDuKienGiaoHang;
	}

	public void setNgayDuKienGiaoHang(Date ngayDuKienGiaoHang) {
		this.ngayDuKienGiaoHang = ngayDuKienGiaoHang;
	}

	public Integer getDoiTac() {
		return doiTac;
	}

	public void setDoiTac(Integer doiTac) {
		this.doiTac = doiTac;
	}

	public Long getKhoHang() {
		return khoHang;
	}

	public void setKhoHang(Long khoHang) {
		this.khoHang = khoHang;
	}

	public String getDocStatus() {
		return docStatus;
	}

	public void setDocStatus(String docStatus) {
		this.docStatus = docStatus;
	}

	public int getDoctypeId() {
		return doctypeId;
	}

	public void setDoctypeId(int doctypeId) {
		this.doctypeId = doctypeId;
	}

	public String getProcessed() {
		return processed;
	}

	public void setProcessed(String processed) {
		this.processed = processed;
	}

	public String getIsvanning() {
		return isvanning;
	}

	public void setIsvanning(String isvanning) {
		this.isvanning = isvanning;
	}

	public String getIsscanning() {
		return isscanning;
	}

	public void setIsscanning(String isscanning) {
		this.isscanning = isscanning;
	}

	public String getIsback() {
		return isback;
	}

	public void setIsback(String isback) {
		this.isback = isback;
	}

	public int getAd_client_id() {
		return ad_client_id;
	}

	public void setAd_client_id(int ad_client_id) {
		this.ad_client_id = ad_client_id;
	}

	public long getAd_org_id() {
		return adorgid;
	}

	public void setAd_org_id(long ad_org_id) {
		this.adorgid = ad_org_id;
	}

}
