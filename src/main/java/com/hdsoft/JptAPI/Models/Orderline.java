package com.hdsoft.JptAPI.Models;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "c_orderline")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Orderline {

	
	@Id
	@Column(name = "c_orderline_id")
	private Long id;

	@Column(name = "m_product_id")
	private Long sanPham;

	@Column(name = "qtyentered")
	private Long soLuong;

	@Column(name = "c_uom_id")
	private Long dvt;

	@Column(name = "c_order_id")
	private Long orderID;

	@Column(name = "qtyinvoiced")
	private Double soLuongThucTe;

	@Column(name = "qtycheck")
	private Double qtyslc;

	@Column(name = "qtydelivered")
	private Double qtypallet;
	@Column(name = "ad_org_id")
	private Long adorgid;
	private Integer ctntally;

	private Integer ctnpallet;
	private String lot;

	private String c_orderline_uu;

	@Column(name = "hds1")
	private String orderno;
	@Column(name = "serno")
	private String invoiceno;
	private Long palletno;
	@Column(name = "hds2")
	private String vendorprefix;
	@Column(name = "hds4")
	private Long carton;
	private Timestamp created;
	private Timestamp updated;
	private Long createdby;
	private Long updatedby;
	private Long line;
	private Timestamp dateordered;
	private Long m_warehouse_id;
	private Long c_currency_id;
	private Long c_tax_id;
	private Long priceentered;

	public Long getPriceentered() {
		return priceentered;
	}

	public void setPriceentered(Long priceentered) {
		this.priceentered = priceentered;
	}

	public Long getC_tax_id() {
		return c_tax_id;
	}

	public void setC_tax_id(Long c_tax_id) {
		this.c_tax_id = c_tax_id;
	}

	public Long getC_currency_id() {
		return c_currency_id;
	}

	public void setC_currency_id(Long c_currency_id) {
		this.c_currency_id = c_currency_id;
	}

	public Long getM_warehouse_id() {
		return m_warehouse_id;
	}

	public void setM_warehouse_id(Long m_warehouse_id) {
		this.m_warehouse_id = m_warehouse_id;
	}

	public Timestamp getDateordered() {
		return dateordered;
	}

	public void setDateordered(Timestamp dateordered) {
		this.dateordered = dateordered;
	}

	public Long getLine() {
		return line;
	}

	public void setLine(Long line) {
		this.line = line;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

	public Long getCreatedby() {
		return createdby;
	}

	public void setCreatedby(Long createdby) {
		this.createdby = createdby;
	}

	public Long getUpdatedby() {
		return updatedby;
	}

	public void setUpdatedby(Long updatedby) {
		this.updatedby = updatedby;
	}

	public Long getAdorgid() {
		return adorgid;
	}

	public void setAdorgid(Long adorgid) {
		this.adorgid = adorgid;
	}

	public Long getCarton() {
		return carton;
	}

	public void setCarton(Long carton) {
		this.carton = carton;
	}

	public String getVendorprefix() {
		return vendorprefix;
	}

	public void setVendorprefix(String vendorprefix) {
		this.vendorprefix = vendorprefix;
	}

	public Long getPalletno() {
		return palletno;
	}

	public void setPalletno(Long palletno) {
		this.palletno = palletno;
	}

	public String getInvoiceno() {
		return invoiceno;
	}

	public void setInvoiceno(String invoiceno) {
		this.invoiceno = invoiceno;
	}

	public String getOrderno() {
		return orderno;
	}

	public void setOrderno(String orderno) {
		this.orderno = orderno;
	}

	public String getC_orderline_uu() {
		return c_orderline_uu;
	}

	public void setC_orderline_uu(String c_orderline_uu) {
		this.c_orderline_uu = c_orderline_uu;
	}

	@Column(name = "ad_client_id")
	private Long clientId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSanPham() {
		return sanPham;
	}

	public void setSanPham(Long sanPham) {
		this.sanPham = sanPham;
	}

	public Long getSoLuong() {
		return soLuong;
	}

	public void setSoLuong(Long soLuong) {
		this.soLuong = soLuong;
	}

	public Long getDvt() {
		return dvt;
	}

	public void setDvt(Long dvt) {
		this.dvt = dvt;
	}

	public Long getOrderID() {
		return orderID;
	}

	public void setOrderID(Long orderID) {
		this.orderID = orderID;
	}

	public Double getSoLuongThucTe() {
		return soLuongThucTe;
	}

	public void setSoLuongThucTe(Double soLuongThucTe) {
		this.soLuongThucTe = soLuongThucTe;
	}

	public String getLot() {
		return lot;
	}

	public void setLot(String lot) {
		this.lot = lot;
	}

	public Double getQtyslc() {
		return qtyslc;
	}

	public void setQtyslc(Double qtyslc) {
		this.qtyslc = qtyslc;
	}

	public Integer getCtntally() {
		return ctntally;
	}

	public void setCtntally(Integer ctntally) {
		this.ctntally = ctntally;
	}

	public Integer getCtnpallet() {
		return ctnpallet;
	}

	public void setCtnpallet(Integer ctnpallet) {
		this.ctnpallet = ctnpallet;
	}

	public Double getQtypallet() {
		return qtypallet;
	}

	public void setQtypallet(Double qtypallet) {
		this.qtypallet = qtypallet;
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

}
