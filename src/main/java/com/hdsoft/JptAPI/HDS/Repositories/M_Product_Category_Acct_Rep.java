package com.hdsoft.JptAPI.HDS.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hdsoft.JptAPI.HDS.model.M_Product_Category_AcctModel;

public interface M_Product_Category_Acct_Rep extends JpaRepository<M_Product_Category_AcctModel	, Long>{
	

}
