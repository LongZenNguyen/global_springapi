package com.hdsoft.JptAPI.HDS.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.hdsoft.JptAPI.Models.Order;

public interface C_orderReposotory  extends JpaRepository<Order, Long>{
	public Order findById(long id);
	public List<Order> findByKhoHangAndLoaiChungTu(long khoHang,long loaiChungTu);
	public List<Order> findByAdorgidAndSoChungTuContainingIgnoreCase(long adorgid,String soChungTu);
	public List<Order> findByKhoHangAndLoaiChungTuAndDocStatus(long khoHang,long loaiChungTu,String docStatus);
//	public Order findBySoChungTuAndAdorgid(String soChungTu);
	
	@Query(value = "select c_order_id,documentno,description,c_doctypetarget_id,dateordered,datepromised,c_bpartner_id,m_warehouse_id,docstatus,c_doctype_id,docaction,processed,isvanning,isscanning,c_order_uu,isback,ad_client_id,ad_org_id,createdby,updatedby,created,updated,dateacct,c_bpartner_location_id,c_currency_id"
			+ ",paymentrule,c_paymentterm_id,invoicerule,deliveryrule,freightcostrule,deliveryviarule,priorityrule,m_pricelist_id from c_order c where ad_client_id = 1000003 and ad_org_id=?1 and documentno like 'XKB-%' order by c_order_id desc",nativeQuery = true)
	public List<Order> getDocumentnoMax(Long ad_org_id);
	
}
