package com.hdsoft.JptAPI.HDS.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.hdsoft.JptAPI.HDS.model.m_inoutline;

public interface m_inoutlineRepository extends JpaRepository<m_inoutline	, Long>,CrudRepository<m_inoutline, Long> {
	
	public List<m_inoutline> findAll();
	public m_inoutline findTopByOrderByIdDesc();
	public List<m_inoutline> findByNumbergenLike(String numbergen);
	public m_inoutline findTopByOrderByNumbergenDesc();
	public List<m_inoutline> findByNumbergenIsNotNullAndAdclientid(long adclientid);

	//HDS Base App
	public List<m_inoutline> findByMinoutidIn(List<Long> minoutid);
	public List<m_inoutline> findByMinoutid(long minoutid);
	
	//chi thi
	public List<m_inoutline> findByMinoutidAndMovementqtyGreaterThan(long minoutid,double movementqty);
	@Query(value = "SELECT m1 FROM m_inoutline m1 where c_orderline_id=?1")
	public List<m_inoutline> findAllFocusOrderlineID(long c_orderline_id);
	@Query(value = "SELECT SUM(qtyentered) FROM m_inoutline m1 where c_orderline_id=?1")
	public Double SumByOrderLineID(long c_orderline_id);
	
	//HDS
	//@Query(value="select ",nativeQuery = true)
}
