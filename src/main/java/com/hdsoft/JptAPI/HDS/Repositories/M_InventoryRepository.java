package com.hdsoft.JptAPI.HDS.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hdsoft.JptAPI.HDS.model.M_Inventory;

public interface M_InventoryRepository extends JpaRepository<M_Inventory, Long>{
	public M_Inventory findTopByOrderByDocumentnoDesc();
	public List<M_Inventory> findAllByAdclientid(long adclientid);

	public List<M_Inventory> findByDocumentnoStartsWith(String documentno);
}
