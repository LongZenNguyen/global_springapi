package com.hdsoft.JptAPI.HDS.Repositories;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hdsoft.JptAPI.HDS.model.m_storageonehand;

@Repository
public interface m_storageonhandRepository
		extends CrudRepository<m_storageonehand, Long>, JpaRepository<m_storageonehand, Long> {
	public List<m_storageonehand> findAll();

	public m_storageonehand findByAsiidAndLocatoridAndProductidAndAdclientid(long asiid, long locatorid, long productid,
			long adclientid);

	public List<m_storageonehand> findDistinctByProductidAndAdclientid(long productid, long adclientid);

	public List<m_storageonehand> findDistinctLocatoridByProductidAndAdclientid(long productid, long adclientid);

	public List<m_storageonehand> findByAsiidAndAdclientidAndLocatorid(long asiid, long adclientid, long locatorid);

	public List<m_storageonehand> findByAdclientidAndLocatoridAndAsiidIn(long adclientid, long locatorid, List<Long> asiid);

	public List<m_storageonehand> findByAdclientidAndQtyonhandGreaterThan(long adclientid, long qtyonhand);

	public List<m_storageonehand> findByProductid(long productid);

	public List<m_storageonehand> findByLocatorid(long locatorid);

	public m_storageonehand findByLocatoridAndProductidAndAsiid(long locatorid, long productid, long asiid);

	// id = ad_client_id , tìm kiếm theo client , vị trí và số lượng luôn luôn > 0
	public List<m_storageonehand> findByAdclientidAndLocatoridAndQtyonhandGreaterThan(long Adclientid, long locatorid, long qtyonhand);

	// HDS Base App
	public List<m_storageonehand> findByAdorgidAndAdclientidAndQtyonhandGreaterThan(long adorgid, long adclientid, long qtyonhand);

	public List<m_storageonehand> findByProductidAndQtyonhandGreaterThan(long productid, long qty);

	public List<m_storageonehand> findByLocatoridAndQtyonhandGreaterThan(long locatorid, long qty);

	public m_storageonehand findByLocatoridAndProductid(long locatorid,long productid);
	
	public List<m_storageonehand> findByProductidAndQtyonhand(long productid,long qtyonhand);
	
	public List<m_storageonehand> findByLocatoridAndAdorgidAndQtyonhandGreaterThan(long locatorid,long adorgid,long qtyonhand);
	
	@Query(value = "SELECT m FROM m_storageonhand m WHERE ad_org_id = ?1 and m_product_id = ?2 and qtyonhand > 0 order by created ASC")
	public List<m_storageonehand> findAfterSort(long ad_org_id,long m_product_id);

	@Query(value = "SELECT m FROM m_storageonhand m WHERE ad_org_id = :ad_org_id and m_product_id = :m_product_id and created between :created1 and :created2 and (qtyonhand <= :qty) order by qtyonhand desc",nativeQuery = true)
	public List<m_storageonehand> findCreatedLike(@Param("ad_org_id") long ad_org_id,@Param("m_product_id") long m_product_id,@Param("created1") String created1,@Param("created2") String created2,@Param("qty") long qty);
	
	public List<m_storageonehand> findByProductidAndCreatedBetweenAndQtyonhandGreaterThan(long productid,Timestamp createdbefore,Timestamp createdafter,long qtyonhand);
	
	@Query(value = "select SUM(m.qtyonhand) from m_storageonhand m where m.m_product_id=?1 and qtyonhand>0",nativeQuery = true)
	public long SumqtyTK(long productid);
	
}
