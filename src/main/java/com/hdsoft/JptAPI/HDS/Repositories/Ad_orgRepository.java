package com.hdsoft.JptAPI.HDS.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.hdsoft.JptAPI.HDS.model.Ad_OrgModel;

public interface Ad_orgRepository extends JpaRepository<Ad_OrgModel, Long> {
	public Ad_OrgModel findByNameAndValue(String name,String value);
	public Ad_OrgModel findByNameOrValue(String name,String value);
	@Query(value = "select count(a) from ad_org a where a.description = ?1 and ad_client_id = 1000003",nativeQuery = true)
	public int countLike(String description);
	@Query(value = "select a.description from ad_org a where a.ad_org_id=(select max(a1.ad_org_id) from ad_org a1 where a1.ad_client_id = 1000003 and a1.description like ?1",nativeQuery = true)
	public String getDescriptionMaxID(String description);
	@Query(value = "select a.Description from ad_org a where a.ad_org_id = ?1",nativeQuery =true)
	public String getDesOrg(long ad_org_id);
	
}
