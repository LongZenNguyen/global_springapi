package com.hdsoft.JptAPI.HDS.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hdsoft.JptAPI.HDS.model.m_attributesetinstance;

public interface m_attributesetinstanceRepository extends JpaRepository<m_attributesetinstance, Long>{
	
	public List<m_attributesetinstance> findAll();
	public List<m_attributesetinstance> findByAdclient(long adclient);
	public List<m_attributesetinstance> findByLotAndAdclient(String lot,long adclient);
	public List<m_attributesetinstance> findByLotStartsWith(String lot);
	public m_attributesetinstance findTopByOrderByIdDesc();
	public m_attributesetinstance findById(long id);
}
