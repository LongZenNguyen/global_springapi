package com.hdsoft.JptAPI.HDS.Repositories;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.hdsoft.JptAPI.HDS.model.m_inout;

public interface m_inoutRepository  extends JpaRepository<m_inout, Long>{
	public List<m_inout> findAll();
	public m_inout findTopByOrderByIdDesc();
	public List<m_inout> findByDocumentnoStartingWith(String documentno);
	
	public List<m_inout> findByAdorgidAndAdclientidAndUpdatedBetween(Long adorgid,Long adclientid,Timestamp updated1,Timestamp updated2);
	public List<m_inout> findByUpdatedBetween(Timestamp updated1,Timestamp updated2);
	public List<m_inout> findByAdclientidAndDocumentnoContainingIgnoreCase(long adorgid,String documentno);
	
	//get
	public List<m_inout> findTop100ByMovementtypeAndAdorgid(String movementtype,long adorgid);
	public List<m_inout> findByAdorgidAndCreatedBetween(Long adorgid,Timestamp date1,Timestamp date2);
	public List<m_inout> findByAdorgidAndMovementtypeAndDocstatus(long adorgid,String movementtype,String docstatus);
	
	@Query(value = "Select m.documentno from m_inout m where m.m_inout_id = (select max(m1.m_inout_id) from m_inout m1 where m1.ad_client_id=?1 and m1.documentno like ?2)",nativeQuery = true)
	public String documentnoMaxID(Long ad_client_id,String pattern);
}
