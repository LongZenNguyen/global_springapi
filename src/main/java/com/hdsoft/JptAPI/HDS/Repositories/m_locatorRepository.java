package com.hdsoft.JptAPI.HDS.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hdsoft.JptAPI.HDS.model.m_locator;

public interface m_locatorRepository extends JpaRepository<m_locator	, Long> {
	public m_locator findTopByOrderByIdDesc();
	public List<m_locator> findByValue(String value);
	public m_locator findById(long id);
	public List<m_locator> findAllByAdclientid(long adclientid);
	public m_locator findByIdAndAdorgid(Long id,Long adorgid);
	public m_locator findByAdclientidAndValueAndMwarehouseid(long adclientid,String value,long mwarehouseid);
	//HDS Base App
	public List<m_locator> findByAdorgid(long adorgid);
	public m_locator findByAdorgidAndValue(long adorgid,String value);
}
