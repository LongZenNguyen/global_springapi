package com.hdsoft.JptAPI.HDS.Repositories;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.hdsoft.JptAPI.HDS.model.*;

public interface m_transactionRepository extends JpaRepository<m_transaction, Long> {
	
	public List<m_transaction> findByOrderByIdDesc();
	public List<m_transaction> findByIdGreaterThan(long Id);
	public m_transaction findTopByOrderByIdDesc(); // Lấy ra record có ID cao nhất
//	public List<m_transaction> findAllByAd_client_id(long ad_client_id);
	//HDS Base App
	public List<m_transaction> findTop100ByMproductidOrderByCreatedDesc(long mproductid);
	public List<m_transaction> findTop100ByAdclientidAndAdorgidAndMproductidOrderByCreatedDesc(long adclientid,long adorgid,long mproductid);
	@Query(value ="select SUM(m.movementqty) from m_transaction m where m.m_product_id = ?1 and m.m_locator_id = ?2 and m.movementtype = ?3 and m.movementdate >= ?4",nativeQuery = true)
	public long qtyFocusMovementtype(long productid,long locatorid,String movementtype,Timestamp date);
	@Query(value = "select sum(m.movementqty) from m_transaction m where m_product_id = ?1",nativeQuery = false)
	public long sumQ(String queryString );
	@Query(value = "select m.m_transaction_id,m.ad_client_id,m.ad_org_id,m.created,m.createdby,m.updated,m.updatedby,m.m_locator_id,m.m_product_id,m.movementdate,m.movementqty,m.m_inoutline_id,m.movementtype,m.m_attributesetinstance_id,m.m_transaction_uu from m_transaction m where m_product_id = ?1 and m.m_locator_id = ?2 and m.movementtype = ?3 ",nativeQuery = true)
	public List<m_transaction> getListTran(long productid,long locatorid,String movementtype);
}
