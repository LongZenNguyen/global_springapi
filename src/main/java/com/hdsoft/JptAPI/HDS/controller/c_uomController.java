package com.hdsoft.JptAPI.HDS.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hdsoft.JptAPI.HDS.Repositories.Ad_orgRepository;
import com.hdsoft.JptAPI.HDS.Repositories.C_UomRepository;
import com.hdsoft.JptAPI.HDS.model.C_UOM;

@RestController
@RequestMapping("api/v1/uom")
public class c_uomController {
	@Autowired
	C_UomRepository cur ;
	@Autowired  
	Ad_orgRepository aor;
	GetIDUUDate g = new GetIDUUDate();
	//HDS Base App
	//GET
	@GetMapping
	@RequestMapping("listallbyorg")
	public List<C_UOM> findAllByOrg(@RequestParam long adorgid){
		String orgDes="";
		try {
			 orgDes = aor.getDesOrg(adorgid);
		} catch (Exception e) {
			// TODO: handle exception
		}
		List<C_UOM> litshowC_UOMs = new ArrayList<C_UOM>();
		litshowC_UOMs =  cur.findByAdorgid(adorgid);
		for (C_UOM c_UOM : litshowC_UOMs) {
			c_UOM.setName(c_UOM.getName().replaceAll("-"+orgDes, ""));
		}
		return litshowC_UOMs;
	}
	
	//POST
	@PostMapping
	@RequestMapping("create")
	public C_UOM createNew(@RequestParam long adorgid,@RequestParam String name,@RequestParam String symbol) {
		String orgDes = aor.getDesOrg(adorgid);
		C_UOM c = new C_UOM();
		c.setAd_client_id(1000003);
		c.setAdorgid(adorgid);
		c.setC_uom_id(g.getNextID("C_UOM"));
		c.setC_uom_uu(g.getUUID());
		c.setCreated(g.getDate());
		c.setUpdated(g.getDate());
		c.setX12de355(symbol);// ký hiệu của dvt
		c.setCreatedby(100);
		c.setStdprecision(2);
		c.setName(name+"-"+orgDes);
		c.setUpdatedby(100);
		c.setCostingprecision(0);
		return cur.saveAndFlush(c);
	}
}
