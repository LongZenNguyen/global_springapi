package com.hdsoft.JptAPI.HDS.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hdsoft.JptAPI.HDS.Repositories.Ad_orgRepository;
import com.hdsoft.JptAPI.HDS.Repositories.C_UomRepository;
import com.hdsoft.JptAPI.HDS.Repositories.M_AttributesetRepository;
import com.hdsoft.JptAPI.HDS.Repositories.M_MovementRepository;
import com.hdsoft.JptAPI.HDS.Repositories.M_ProductPriceRepository;
import com.hdsoft.JptAPI.HDS.Repositories.M_Product_CategoryCRepository;
import com.hdsoft.JptAPI.HDS.Repositories.m_inoutRepository;
import com.hdsoft.JptAPI.HDS.Repositories.m_inoutlineRepository;
import com.hdsoft.JptAPI.HDS.Repositories.m_locatorRepository;
import com.hdsoft.JptAPI.HDS.Repositories.m_productRepository;
import com.hdsoft.JptAPI.HDS.Repositories.m_storageonhandRepository;
import com.hdsoft.JptAPI.HDS.Repositories.m_transactionRepository;
import com.hdsoft.JptAPI.HDS.model.C_UOM;
import com.hdsoft.JptAPI.HDS.model.C_orderLineInformation;
import com.hdsoft.JptAPI.HDS.model.ProductInfomation;
import com.hdsoft.JptAPI.HDS.model.Product_TonCuoiKy;
import com.hdsoft.JptAPI.HDS.model.m_inout;
import com.hdsoft.JptAPI.HDS.model.m_inoutline;
import com.hdsoft.JptAPI.HDS.model.m_product;
import com.hdsoft.JptAPI.HDS.model.m_productprice;
import com.hdsoft.JptAPI.HDS.model.m_storageonehand;
import com.hdsoft.JptAPI.HDS.model.m_transaction;
import com.hdsoft.JptAPI.Models.Orderline;
import com.hdsoft.JptAPI.Repositories.OrderlineRepository;

@RestController
@RequestMapping("api/v1/sanpham")
public class ProductInformationController {
	@Autowired
	m_productRepository mp;
	@Autowired
	C_UomRepository cur;
	@Autowired
	M_Product_CategoryCRepository mrcr;
	@Autowired
	M_AttributesetRepository mar;

	@Autowired
	M_ProductPriceRepository mppr;
	@Autowired
	m_storageonhandRepository msr;
	@Autowired
	m_inoutRepository mio;
	@Autowired
	m_inoutlineRepository miol;
	@Autowired
	m_locatorRepository ml;
	@Autowired
	OrderlineRepository or;
	@Autowired
	m_transactionRepository mt;
	@Autowired
	M_MovementRepository mm;
	@Autowired
	Ad_orgRepository aor;
	GetIDUUDate g = new GetIDUUDate();
	ProcessController Pc = new ProcessController();

	// HDS Base App
	// GET
	// Chỉ thị lấy hàng HDS Base khi vừa tạo kế hoạch
	@GetMapping
	@RequestMapping("chithixk")
	public List<ProductInfomation> chithiGen(@RequestParam long m_inout_id, @RequestParam long adorgid) {

		List<ProductInfomation> chithi = new ArrayList<ProductInfomation>();
		List<m_storageonehand> chithis = new ArrayList<m_storageonehand>();
		List<Orderline> list1 = new ArrayList<Orderline>();
		list1 = or.findByOrderIDAndSoLuongGreaterThan(m_inout_id, 0);
		for (Orderline m1 : list1) {
			List<m_storageonehand> storageByProductList = new ArrayList<m_storageonehand>();
			storageByProductList = msr.findAfterSort(m1.getAdorgid(), m1.getSanPham());
			if (storageByProductList.size() > 0) {
				while (storageByProductList.size() > 0 && m1.getSoLuong() > 0) {
					List<m_storageonehand> storageByCreated = new ArrayList<m_storageonehand>();
//					System.out.println("While1");
					String create = storageByProductList.get(0).getCreated().toString().substring(0, 10);
					storageByCreated = msr.findByProductidAndCreatedBetweenAndQtyonhandGreaterThan(m1.getSanPham(),
							Timestamp.valueOf(create + " 00:00:00"), Timestamp.valueOf(create + " 23:59:59"), 0);

					for (m_storageonehand mbc : storageByCreated) {
//						} else {

						if (m1.getSoLuong() == 0) {
							break;
						} else {

							if (m1.getSoLuong() < mbc.getQtyonhand()) {
								// ============
								m_storageonehand s11 = new m_storageonehand();
								s11 = msr.findByLocatoridAndProductid(1033993, 1238942);
								// ===============
								m_storageonehand mbcsub = new m_storageonehand();
								// Điền thông tin cho đối tượng add
								mbcsub.setAd_org_id(mbc.getAd_org_id());
								mbcsub.setCreatedby(mbc.getCreatedby());
								mbcsub.setLocatorid(mbc.getLocatorid());
								mbcsub.setProductid(mbc.getProductid());
								mbcsub.setQtyonhand(m1.getSoLuong());
								mbcsub.setUpdatedby(mbc.getUpdatedby());
								// End
								m1.setSoLuong((long) 0);
								chithis.add(mbcsub);
								// ============check qty get and qty update;
								s11 = msr.findByLocatoridAndProductid(1033993, 1238942);
								// ===============
								break;
							} else {
								chithis.add(mbc);
								//
								storageByProductList.remove(mbc);
								m1.setSoLuong(m1.getSoLuong() - mbc.getQtyonhand());
								if (m1.getSoLuong() == 0) {
									break;
								}
							}
						}

//						}
					}
				}

			}
		}
		String orgDes="";
		if (chithis.size()>0) {
			orgDes = aor.getDesOrg(chithis.get(0).getAd_org_id());
		}
		
		for (m_storageonehand mst1 : chithis) {
			ProductInfomation p1 = new ProductInfomation();
			p1.setClientid(1000003);
			p1.setOrgid(mst1.getAd_org_id());
			p1.setDvt(cur.getOne(mp.getOne(mst1.getProductid()).getC_uom_id()).getName().replaceAll("-"+orgDes, ""));
			p1.setLocatorid(mst1.getLocatorid());
			p1.setLocatorName(ml.getOne(mst1.getLocatorid()).getValue().replaceAll("-"+orgDes, ""));
			p1.setProductID(mst1.getProductid());
			p1.setProductValue(mp.getOne(mst1.getProductid()).getValue().replaceAll("-"+orgDes, ""));
			p1.setQty(mst1.getQtyonhand());
			p1.setCreated((Timestamp) mst1.getCreated());
			p1.setTotalQtysale((p1.getQty() * mppr.findByProductid(p1.getProductID()).get(0).getPricelist()));
			p1.setC_orderline_id(or.findByOrderIDAndSanPham(m_inout_id, p1.getProductID()).get(0).getId());
			chithi.add(p1);
		}

		return chithi;
	}

	// Chỉ thị lấy hàng khi coi lại kh
	public List<C_orderLineInformation> detailKHXH(long c_order_id) {
		List<C_orderLineInformation> listOrderlines = new ArrayList<C_orderLineInformation>();
		String orgDes ="";
		for (Orderline o1 : or.findByOrderID(c_order_id)) {
			orgDes = aor.getDesOrg(o1.getAdorgid());
			C_orderLineInformation c1 = new C_orderLineInformation();
			c1.setC_orderline_id(o1.getId());
			c1.setM_product_id(o1.getSanPham());
			c1.setQtyentered(o1.getSoLuong());

			try {
				c1.setQtyreceived(miol.SumByOrderLineID(o1.getId()));
			} catch (Exception e) {
				c1.setQtyreceived(0);
			}

			double confirmStatus;
			confirmStatus = o1.getSoLuong() - c1.getQtyreceived();
			c1.setQtythieu(confirmStatus);
			if (confirmStatus > 0) {
				c1.setStatus("Thiếu " + confirmStatus);
			} else if (confirmStatus == 0) {
				c1.setStatus("Đủ");
			} else {
				c1.setStatus("Thừa " + (confirmStatus * (-1)));
			}
			c1.setProductvalue(mp.getOne(o1.getSanPham()).getValue().replaceAll("-"+orgDes, ""));
			listOrderlines.add(c1);

		}

		return listOrderlines;
	}

	@GetMapping
	@RequestMapping("chithixkl2")
	public List<ProductInfomation> chithiGenLan2(@RequestParam long m_inout_id, @RequestParam long adorgid) {
		String orgDes = aor.getDesOrg(adorgid);
		List<ProductInfomation> chithi = new ArrayList<ProductInfomation>();
		List<m_storageonehand> chithis = new ArrayList<m_storageonehand>();
		List<Orderline> list1 = new ArrayList<Orderline>();
		// list1 = or.findByOrderIDAndSoLuongGreaterThan(m_inout_id, 0);
		// lấy lại dữ liệu còn thiếu để convert sang orderline chuẩn
		// List<C_orderLineInformation> listConvert = new
		// ArrayList<C_orderLineInformation>();
		for (C_orderLineInformation c1 : detailKHXH(m_inout_id)) {
			Orderline o1 = new Orderline();
			o1.setId(c1.getC_orderline_id());
			o1.setOrderID(m_inout_id);
			o1.setSanPham(c1.getM_product_id());
			o1.setSoLuong((long) (c1.getQtyentered() - c1.getQtyreceived()));
			System.out.println("SL can lay tiep: " + o1.getSoLuong());
			list1.add(o1);
		}
		for (Orderline m1 : list1) {
			List<m_storageonehand> storageByProductList = new ArrayList<m_storageonehand>();
			storageByProductList = msr.findAfterSort(mp.getOne(m1.getSanPham()).getAd_org_id(), m1.getSanPham());
			if (storageByProductList.size() > 0) {
				while (storageByProductList.size() > 0 && m1.getSoLuong() > 0) {
					List<m_storageonehand> storageByCreated = new ArrayList<m_storageonehand>();
//					System.out.println("While1");
					String create = storageByProductList.get(0).getCreated().toString().substring(0, 10);
					storageByCreated = msr.findByProductidAndCreatedBetweenAndQtyonhandGreaterThan(m1.getSanPham(),
							Timestamp.valueOf(create + " 00:00:00"), Timestamp.valueOf(create + " 23:59:59"), 0);

					for (m_storageonehand mbc : storageByCreated) {
//						} else {

						if (m1.getSoLuong() == 0) {
							break;
						} else {

							if (m1.getSoLuong() < mbc.getQtyonhand()) {
								// ============
								m_storageonehand s11 = new m_storageonehand();
								s11 = msr.findByLocatoridAndProductid(1033993, 1238942);
								// ===============
								m_storageonehand mbcsub = new m_storageonehand();
								// Điền thông tin cho đối tượng add
								mbcsub.setAd_org_id(mbc.getAd_org_id());
								mbcsub.setCreatedby(mbc.getCreatedby());
								mbcsub.setLocatorid(mbc.getLocatorid());
								mbcsub.setProductid(mbc.getProductid());
								mbcsub.setQtyonhand(m1.getSoLuong());
								mbcsub.setUpdatedby(mbc.getUpdatedby());
								// End
								m1.setSoLuong((long) 0);
								chithis.add(mbcsub);
								// ============check qty get and qty update;
								s11 = msr.findByLocatoridAndProductid(1033993, 1238942);
								// ===============
								break;
							} else {
								chithis.add(mbc);
								//
								storageByProductList.remove(mbc);
								m1.setSoLuong(m1.getSoLuong() - mbc.getQtyonhand());
								if (m1.getSoLuong() == 0) {
									break;
								}
							}
						}

//						}
					}
				}

			}
		}

		for (m_storageonehand mst1 : chithis) {
			ProductInfomation p1 = new ProductInfomation();
			p1.setClientid(1000003);
			p1.setOrgid(mst1.getAd_org_id());
			p1.setDvt(cur.getOne(mp.getOne(mst1.getProductid()).getC_uom_id()).getName().replaceAll("-"+orgDes, ""));
			p1.setLocatorid(mst1.getLocatorid());
			p1.setLocatorName(ml.getOne(mst1.getLocatorid()).getValue().replaceAll("-"+orgDes, ""));
			p1.setProductID(mst1.getProductid());
			p1.setProductValue(mp.getOne(mst1.getProductid()).getValue().replaceAll("-"+orgDes, ""));
			p1.setQty(mst1.getQtyonhand());
			p1.setCreated((Timestamp) mst1.getCreated());
			p1.setTotalQtysale((p1.getQty() * mppr.findByProductid(p1.getProductID()).get(0).getPricelist()));
			p1.setC_orderline_id(or.findByOrderIDAndSanPham(m_inout_id, p1.getProductID()).get(0).getId());
			chithi.add(p1);
		}

		return chithi;
	}

	// =================================

	@GetMapping
	@RequestMapping("test1")
	public double abc() {
		return mppr.findByProductid(1247706).get(0).getPricelist();
	}

	@PostMapping
	@RequestMapping("processctxk")
	public String processStorageonhand(@RequestParam long locatorid, @RequestParam long productid,
			@RequestParam long adorgid, @RequestParam long qty) {
		processFuncXuatHang(locatorid, productid, adorgid, qty);
		return "Successful!";
	}

	// ===============================
	public String processFuncXuatHang(@RequestParam long locatorid, @RequestParam long productid,
			@RequestParam long adorgid, @RequestParam long qty) {
		try {
			long locatortoXH;
			locatortoXH = ml.findByAdorgidAndValue(adorgid, "Xuất hàng").getId();

			processStorage(adorgid, locatorid, locatortoXH, productid, qty);
			System.out.println("Xu ly so lieu xong");
			createTransaction(productid, locatorid, qty * (-1), adorgid, "M-");
			createTransaction(productid, locatortoXH, qty, adorgid, "M+");
			System.out.println("tran 2");
		} catch (Exception e) {
			System.out.println("error:" + e);
		}
		return "Successfully!";
	}

	public String createTransaction(long m_product_id, long locatorid, long qty, long adorgid, String movementtype) {
		m_transaction m_transaction = new m_transaction();
		m_transaction.setAd_client_id((long) 1000003);
		m_transaction.setAd_org_id(adorgid);
		m_transaction.setCreated(g.getDate());
		m_transaction.setCreatedby((long) 100);
		m_transaction.setId(g.getNextID("M_Transaction"));
		m_transaction.setM_attributesetinstance_id((long) 0);
		m_transaction.setM_inoutline_id(null);
		m_transaction.setM_locator_id(locatorid);
		m_transaction.setM_product_id(m_product_id);
		m_transaction.setM_transaction_uu(g.getUUID());
		m_transaction.setMovementdate(g.getDate());
		m_transaction.setMovementqty(qty);
		m_transaction.setMovementtype(movementtype);
		m_transaction.setUpdated(g.getDate());
		m_transaction.setUpdatedby((long) 100);
		mt.save(m_transaction);
		return "Sucessful!";
	}

	@GetMapping
	@RequestMapping("abc")
	public List<m_storageonehand> a1() {
		List<m_storageonehand> list = new ArrayList<m_storageonehand>();
		list.add(msr.findByLocatoridAndProductid(1033993, 1238942));
		if (msr.findByLocatoridAndProductid(1033993, 1238942) == null) {
			System.out.println("Null");
		}
		return list;
	}

//	@PostMapping
//	@RequestMapping("processqty")
	public String processStorage(long adorgid, long locatorid, long locatortoid, long productid, long qty) {

		m_storageonehand s11 = new m_storageonehand();
		System.out.println("LocatorID: " + locatorid + " locatorToID: " + locatortoid);
		//

		s11 = msr.findByLocatoridAndProductid(locatorid, productid);
		System.out.println("Hasagi");
		System.out.println("M_Storage In Old Locator: " + s11.getLocatorid() + "-" + s11.getProductid() + "-"
				+ s11.getQtyonhand());
		System.out.println("Hasayo");

		long oldqty1;
		oldqty1 = s11.getQtyonhand();

		System.out.println("QtyOnHand: " + oldqty1);
		m_storageonehand storageonehand = new m_storageonehand();
		m_storageonehand s1 = new m_storageonehand();// chứa dữ liệu tồn cũ của sp

		//
		long oldqty;
//		System.out.println(s1.getLocatorid()+"");
		// check vị trí tới đã có chứa sp và slg hay chưa để lựa chọn update hay insert
		if (msr.findByLocatoridAndProductid(locatortoid, productid) != null) {
			// đã có record
			// cộng slg vào vị trí mới
			storageonehand = msr.findByLocatoridAndProductid(locatortoid, productid);
			oldqty = storageonehand.getQtyonhand();
			storageonehand.setQtyonhand((oldqty + qty));
			System.out.println("before  save");
			msr.save(storageonehand);
			System.out.println("After save");
			System.out.println("Cộng số liệu thành công!");

		} else {
			// Không có record
			// Tạo record và cộng slg vị trí mới
			storageonehand.setAd_org_id(adorgid);
			storageonehand.setAsiid((long) 0);
			storageonehand.setCreated(g.getDate());
			storageonehand.setCreatedby(100);
			storageonehand.setDatematerialpolicy(Timestamp.valueOf(java.time.LocalDate.now() + " 00:00:00"));
			storageonehand.setId(1000003);
			storageonehand.setLocatorid(locatortoid);
			storageonehand.setM_storageonhand_uu(g.getUUID());
			storageonehand.setProductid(productid);
			storageonehand.setQtyonhand(qty);
			storageonehand.setUpdated(g.getDate());
			storageonehand.setUpdatedby(100);
			msr.save(storageonehand);
			System.out.println("Tạo record thành công!");
		} // trừ slg từ vị trí cũ
		s11.setQtyonhand(oldqty1 - qty);
		System.out.println("Old qty before : " + oldqty1 + " qty sub: " + qty + " new qty: " + s11.getQtyonhand());
		msr.save(s11);
		System.out.println("Trừ số liệu thành công!");
		return "Sucessful!";
	}

	// =============================

	@GetMapping
	@RequestMapping("t")
	public List<m_storageonehand> abc(@RequestParam long productid, @RequestParam long qty) {
		List<m_storageonehand> list = new ArrayList<m_storageonehand>();
		list = msr.findByProductidAndQtyonhand(productid, qty);
		return list;

	}

	@GetMapping
	@RequestMapping("/thongtin")
	public ProductInfomation ThongTinSP(@RequestParam long productID) {
		String orgDes;
		ProductInfomation p = new ProductInfomation();
		m_product mpr = new m_product();
		mpr = mp.findById(productID);
		orgDes = aor.getDesOrg(mpr.getAd_org_id());
		p.setClientid(mpr.getClientId());
		p.setDvt(cur.getOne(mpr.getC_uom_id()).getName().replaceAll("-"+orgDes, ""));
//		p.setM_attributeset_id(mpr.getM_attributeset_id());
		p.setOrgid(mpr.getAd_org_id());
		p.setProductID(mpr.getId());
		p.setProductName(mpr.getName().replaceAll("-"+orgDes, ""));
		p.setProductValue(mpr.getValue().replaceAll("-"+orgDes, ""));
		p.setUnisperpack(mpr.getUnitsperpack());
		p.setUomID(mpr.getC_uom_id());
		p.setProductCategoryid(mpr.getM_product_category());
		m_productprice mProductprice = new m_productprice();
		try {
			mProductprice = mppr.findByProductid(mpr.getId()).get(0);
		} catch (Exception e) {
			mProductprice.setPricelist(0);
			mProductprice.setPricestd(0);
		}
		p.setPricesale(mProductprice.getPricelist());
		p.setPurprice(mProductprice.getPricestd());
		p.setProductCategoryName(mrcr.getOne(mpr.getM_product_category()).getValue().replaceAll("-"+orgDes, ""));
		return p;
	}

	@GetMapping
	@RequestMapping("/thongtin1")
	public ProductInfomation ThongTinSP1(@RequestParam long productID) {
		ProductInfomation p = new ProductInfomation();
		m_product mpr = new m_product();
		mpr = mp.findById(productID);
		String orgDes = aor.getDesOrg(mpr.getAd_org_id());
		p.setClientid(mpr.getClientId());
		p.setDvt(cur.getOne(mpr.getC_uom_id()).getName().replaceAll("-"+orgDes, ""));
//		p.setM_attributeset_id(mpr.getM_attributeset_id());
		p.setOrgid(mpr.getAd_org_id());
		p.setProductID(mpr.getId());
		p.setProductName(mpr.getName().replaceAll("-"+orgDes, ""));
		p.setProductValue(mpr.getValue().replaceAll("-"+orgDes, ""));
		p.setUnisperpack(mpr.getUnitsperpack());
		p.setUomID(mpr.getC_uom_id());
		p.setProductCategoryid(mpr.getM_product_category());
		m_productprice mProductprice = new m_productprice();
		try {
			mProductprice = mppr.findByProductid(mpr.getId()).get(0);
		} catch (Exception e) {
			mProductprice.setPricelist(0);
			mProductprice.setPricestd(0);
		}
		p.setPricesale(mProductprice.getPricelist());
		p.setPurprice(mProductprice.getPricestd());
		p.setProductCategoryName(mrcr.getOne(mpr.getM_product_category()).getValue());
//		p.setNote(note);
		return p;
	}

	@GetMapping
	@RequestMapping("1")
	public C_UOM aj(@RequestParam long id) {
		return cur.getOne(id);
	}

	@GetMapping
	@RequestMapping("findbyvalueandorg")
	public ProductInfomation findbyvalueandorg(@RequestParam String value, @RequestParam long orgid) {
		ProductInfomation p = new ProductInfomation();
		m_product m = new m_product();
		String orgDes = aor.getDesOrg(orgid);
		
		m = mp.findByValueAndAdorgid(value+"-"+orgDes, orgid);
		try {

			System.out.println("Client: " + m.getAd_org_id());
			if (m.getAd_org_id() == null) {
				System.out.println("null");
				return null;
			} else {
				p.setClientid(1000003);
				p.setDvt(cur.findById(m.getC_uom_id()).get().getName());
				p.setM_attributeset_id(m.getM_attributeset_id());
				p.setOrgid(orgid);
				p.setProductCategoryid(m.getM_product_category());
				p.setProductCategoryName(mrcr.getOne(m.getM_product_category()).getValue());
				p.setProductID(m.getId());
				p.setProductName(m.getName().replaceAll("-"+orgDes, ""));
				p.setProductValue(m.getValue().replaceAll("-"+orgDes, ""));
//				p.setThuoctinh(mar.getOne(m.getM_attributeset_id()).getName());
				p.setUnisperpack(1);
				p.setUomID(m.getC_uom_id());
				m_productprice mProductprice = new m_productprice();
				try {
					mProductprice = mppr.findByProductid(m.getId()).get(0);
				} catch (Exception e) {
					mProductprice.setPricelist(0);
					mProductprice.setPricestd(0);
				}
				p.setPricesale(mProductprice.getPricelist());
				p.setPurprice(mProductprice.getPricestd());
				return p;
			}
		} catch (Exception e) {
			return null;
		}
	}

	@GetMapping
	@RequestMapping("testz")
	public m_productprice a() {
		return mppr.findByProductid(1237939).get(0);
	}

	@GetMapping
	@RequestMapping("thongtintatcasanpham")
	public List<ProductInfomation> listAll(@RequestParam long adorgid) {
		String orgDes=aor.getDesOrg(adorgid);
		List<ProductInfomation> list = new ArrayList<ProductInfomation>();
		for (m_product m : mp.findByAdorgid(adorgid)) {
			ProductInfomation p = new ProductInfomation();
			p.setDvt(cur.getOne(m.getC_uom_id()).getName());
			try {
				p.setM_attributeset_id(m.getM_attributeset_id());
			} catch (Exception e) {
				p.setM_attributeset_id(null);
			}

			p.setProductCategoryid(m.getM_product_category());
			p.setProductID(m.getId());
			p.setProductName(m.getName().replaceAll("-"+orgDes, ""));
			p.setProductValue(m.getValue().replaceAll("-"+orgDes, ""));
			p.setUomID(m.getC_uom_id());
//			p.setThuoctinh(mar.getOne(m.getM_attributeset_id()).getName());
			p.setProductCategoryName(mrcr.getOne(m.getM_product_category()).getValue());
			m_productprice mProductprice = new m_productprice();
//			System.out.println("AHAHAHA " + m.getId());
			try {
				mProductprice = mppr.findByProductid(m.getId()).get(0);
			} catch (Exception e) {
				mProductprice.setPricelist(0);
				mProductprice.setPricestd(0);
			}

			p.setPricesale(mProductprice.getPricelist());
			p.setPurprice(mProductprice.getPricestd());
			System.out.println("id: "+m.getId());
			try {
				p.setQty(msr.SumqtyTK(m.getId()));
			} catch (Exception e) {
				p.setQty(0);
			}
			
			System.out.println("qtyL "+p.getQty());
			list.add(p);

		}
		return list;
	}

	// Top bán chạy=

	public List<m_inout> getmInOutBetween(long adorgid, String dateFrom, String dateTo, String movementtype) {
		List<m_inout> list = new ArrayList<m_inout>();
		for (m_inout m1 : mio.findByAdorgidAndMovementtypeAndDocstatus(adorgid, movementtype, "DR")) {

			if (m1.getCreated().after(Timestamp.valueOf(dateFrom + " 00:00:00"))
					&& m1.getCreated().before(Timestamp.valueOf(dateTo + " 23:59:59"))) {
				System.out.println(m1.getCreated());
				list.add(m1);
			} else {
			}
		}
		return list;
	}

	@GetMapping
	@RequestMapping("banchay")
	public List<ProductInfomation> banchay(@RequestParam long adorgid, @RequestParam String dateFrom,
			@RequestParam String dateTo, @RequestParam String movementtype) {
		List<ProductInfomation> list = new ArrayList<ProductInfomation>();
		List<m_inout> list2 = new ArrayList<m_inout>();
		list2 = getmInOutBetween(adorgid, dateFrom, dateTo, movementtype);
		List<m_inoutline> list3 = new ArrayList<m_inoutline>();
		String orgDes = aor.getDesOrg(adorgid);
		// get all inoutline
		for (m_inout miom : list2) {
			list3.addAll(miol.findByMinoutid(miom.getId()));
		}
		System.out.println("Size list3: " + list3.size());
		// bắt đầu xử lý số liệu
		for (m_inoutline mi1 : list3) {
			int count = 0;
			int oldqty = 0;
			int location = 0;
			for (ProductInfomation p2 : list) {
				if (mi1.getM_product_id() == p2.getProductID()) {
					count++;
					break;
				} else {
					location++;
				}
			}
			if (count == 0) {
				ProductInfomation p1 = new ProductInfomation();
				p1.setClientid(1000003);
				p1.setDvt(cur.getOne(mi1.getC_uom_id()).getName());
				p1.setM_attributeset_id(mp.getOne(mi1.getM_product_id()).getM_attributeset_id());
				p1.setOrgid(adorgid);
				p1.setPricesale(mppr.findByProductid(mi1.getM_product_id()).get(0).getPricelist());
				p1.setPurprice(mppr.findByProductid(mi1.getM_product_id()).get(0).getPricestd());
				p1.setProductID(mi1.getM_product_id());
				p1.setProductName(mp.getOne(mi1.getM_product_id()).getName().replaceAll("-"+orgDes, ""));
				p1.setProductValue(mp.getOne(mi1.getM_product_id()).getValue().replaceAll("-"+orgDes, ""));
				p1.setQty((long) mi1.getQtyentered());
				p1.setUomID(mp.getOne(mi1.getM_product_id()).getC_uom_id());
				System.out.println("qty: " + p1.getQty() + " price sales: " + p1.getPricesale());
				if (movementtype.equals("C-")) {
					p1.setTotalQtysale(p1.getQty() * p1.getPricesale());
				} else {
					p1.setTotalqtypur(p1.getQty() * p1.getPurprice());
				}
				list.add(p1);
			} else {
				list.get(location).setQty((long) (list.get(location).getQty() + mi1.getQtyentered()));
			}
		}
		return list;
	}
	// Bao cao ton theo ky

	@GetMapping
	@RequestMapping("toncuoiky")
	public List<Product_TonCuoiKy> listTonCuoiKy(@RequestParam String date, @RequestParam long adorgid) {
		List<Product_TonCuoiKy> p1 = new ArrayList<Product_TonCuoiKy>();
		List<m_storageonehand> m1 = new ArrayList<m_storageonehand>();
		m1 = msr.findByAdorgidAndAdclientidAndQtyonhandGreaterThan(adorgid, 1000003, (long) 0);
		String orgDes = aor.getDesOrg(adorgid);
		System.out.println("Size: " + m1.size());
		for (m_storageonehand m2 : m1) {
			Product_TonCuoiKy p2 = new Product_TonCuoiKy();
			p2.setLocatorid(m2.getLocatorid());
			p2.setLocatorName(ml.getOne(m2.getLocatorid()).getValue().replaceAll("-"+orgDes, ""));
			p2.setPriductid(m2.getProductid());
			p2.setProductName(mp.getOne(m2.getProductid()).getValue().replaceAll("-"+orgDes, ""));
			// System.out.println("select SUM(movementqty) from M_Transaction where
			// m_product_id="+m2.getProductid()+" and m_locator_id="+m2.getLocatorid()+" and
			// movementtype='V-' and movementdate between '"+date+" 00:00:00' and now()");
			// String queryString = +m2.getProductid()+" and
			// m_locator_id="+m2.getLocatorid()+" and movementtype='C-' and (movementdate
			// between '"+Timestamp.valueOf(date+" 00:00:00")+"' and now())";
			// System.out.println(queryString);
			long qtyXH = 0, qtyNH = 0;
			//date = date + " 01:02:20.206788";
			for (m_transaction mt1 : mt.getListTran(m2.getProductid(), m2.getLocatorid(), "V+")) {
				if (mt1.getMovementdate().compareTo(Date.valueOf(date))>0) {
					qtyNH += mt1.getMovementqty();
				}
			}
			System.out.println(qtyNH);
			System.out.println("m_productid: "+m2.getProductid()+"_ mlocatorid="+m2.getLocatorid());
			for (m_transaction mt1 : mt.getListTran(m2.getProductid(), m2.getLocatorid(), "C-")) {
				System.out.println("Date 1: "+mt1.getMovementdate()+" DATE 2: "+date);
				if (mt1.getMovementdate().compareTo(Date.valueOf(date))>0) {
					qtyXH += mt1.getMovementqty();
				}
			}
			System.out.println(qtyXH);

			p2.setQty(m2.getQtyonhand() + qtyXH - qtyNH);
			if (p2.getQty()>0) {
				p1.add(p2);
			}
			
		}

		return p1;
	}
}
