package com.hdsoft.JptAPI.HDS.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hdsoft.JptAPI.HDS.Repositories.Ad_orgRepository;
import com.hdsoft.JptAPI.HDS.Repositories.C_Bp_GroupRepository;
import com.hdsoft.JptAPI.HDS.model.C_Bp_GroupModel;

@RestController
@RequestMapping("/api/v1/cbgroup")
public class C_Bp_GroupController {
	@Autowired
	C_Bp_GroupRepository cbgr;
	@Autowired  
	Ad_orgRepository aor;
	GetIDUUDate g = new GetIDUUDate();
	//HDS Base App
	//GET
	@GetMapping
	@RequestMapping("findbyorg")
	public List<C_Bp_GroupModel> getAllByOrg(@RequestParam long adorgid){
		String orgDes = aor.getDesOrg(adorgid);
		List<C_Bp_GroupModel> litsh1 = new ArrayList<C_Bp_GroupModel>();
		litsh1=cbgr.findByAdorgid(adorgid);
		for (C_Bp_GroupModel c_Bp_GroupModel : litsh1) {
			c_Bp_GroupModel.setName(c_Bp_GroupModel.getName().replaceAll("-"+orgDes, ""));
			c_Bp_GroupModel.setValue(c_Bp_GroupModel.getValue().replaceAll("-"+orgDes, ""));
		}
		return litsh1;
	}
	//POST
	@PostMapping
	@RequestMapping("create")
	public C_Bp_GroupModel createCBG(@RequestParam String name,@RequestParam String value,@RequestParam long adorgid) {
		String orgDes = aor.getDesOrg(adorgid);
		C_Bp_GroupModel c = new C_Bp_GroupModel();
		c.setAd_client_id(1000003);
		c.setAdorgid(adorgid);
		c.setC_bp_group_uu(g.getUUID());
		c.setCreated(g.getDate());
		c.setCreatedby(100);
		c.setId(g.getNextID("C_BP_Group"));
		c.setName(name+"-"+orgDes);
		c.setValue(value+"-"+orgDes);
		c.setUpdated(g.getDate());
		c.setUpdatedby(100);
		return cbgr.saveAndFlush(c);
	}
}
