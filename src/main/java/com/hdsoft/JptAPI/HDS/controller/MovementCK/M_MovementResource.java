 package com.hdsoft.JptAPI.HDS.controller.MovementCK;

import java.sql.Date;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v2/movement")
public class M_MovementResource {
	

	@PutMapping
	public void updateMovementSuccessfully(@RequestParam("documentNo") String documentNo, @RequestParam("m_movement_id") long m_movement_id) {
		M_MovementService.updateMovementSuccessfully(documentNo, m_movement_id);
		System.out.println("Succesfully !!!!!!");
	}
	
	@PostMapping
	@RequestMapping("/insertmovement")
	public void insertMovement(@RequestParam("documentNo") String documentNo, @RequestParam("movementDate") Date movementDate) {
		M_MovementService.createMovement(documentNo, movementDate);
		System.out.println("Succesfully !!!!!!");
	}
	
	@PostMapping
	@RequestMapping("/demo/insertmovement")
	public void insertMovement(@RequestParam("documentNo") String documentNo, @RequestParam("movementDate") Date movementDate,
			@RequestParam("ad_client_id") Long ad_client_id, @RequestParam("ad_org_id") Long ad_org_id, @RequestParam("ad_user_id") Long ad_user_id) {
		M_MovementService.createMovementDemo(documentNo, movementDate, ad_client_id, ad_org_id, ad_user_id);;
		System.out.println("Succesfully !!!!!!");
	}
	
	@PostMapping
	@RequestMapping("/insertrack")
	public void insertMovementRack(@RequestParam("movementDate") Date movementDate) {
		M_MovementService.createMovementRack(movementDate);
		System.out.println("Succesfully !!!!!!");
	}
	
	@PostMapping
	@RequestMapping("/insertreceiving")
	public void insertMovementReceiving(@RequestParam("movementDate") Date movementDate) {
		M_MovementService.createMovementReceiving(movementDate);
		System.out.println("Succesfully !!!!!!");
	}
	
	@PostMapping
	@RequestMapping("/insertvanning")
	public void insertMovementVanning(@RequestParam("c_order_id") int c_order_id, @RequestParam("documentNo") String documentNo
			, @RequestParam("movementDate") Date movementDate) {
		M_MovementService.createMovementVanning(c_order_id, documentNo, movementDate);
	}
	
	@PostMapping
	@RequestMapping("/insertvanningtorack")
	public void insertMovementVanningToRack(@RequestParam("c_order_id") int c_order_id, @RequestParam("documentNo") String documentNo
			, @RequestParam("movementDate") Date movementDate) {
		M_MovementService.createMovementVanningToRack(c_order_id, documentNo, movementDate);
	}
	
}
