package com.hdsoft.JptAPI.HDS.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hdsoft.JptAPI.HDS.Repositories.Ad_orgRepository;
import com.hdsoft.JptAPI.HDS.Repositories.C_bPartnerRepository;
import com.hdsoft.JptAPI.HDS.Repositories.C_orderReposotory;
import com.hdsoft.JptAPI.HDS.Repositories.M_ProductPriceRepository;
import com.hdsoft.JptAPI.HDS.model.C_OrderInformation;
import com.hdsoft.JptAPI.Models.Order;
import com.hdsoft.JptAPI.Models.Orderline;
import com.hdsoft.JptAPI.Repositories.OrderlineRepository;

@RestController
@RequestMapping("api/v1/c_order")
public class c_orderController {
	@Autowired
	C_orderReposotory cor;
	@Autowired
	M_ProductPriceRepository mpp;
	@Autowired
	OrderlineRepository olr;
	@Autowired
	C_bPartnerRepository c_bPartnerRepository;
	@Autowired  
	Ad_orgRepository aor;
	GetIDUUDate g = new GetIDUUDate();

	// Get
	@GetMapping
	@RequestMapping("getmaxdocumentbyorg")
	public Order getmaxbyorg(@RequestParam long adorgid, @RequestParam String documentnoPatterm) {
		return cor.findByAdorgidAndSoChungTuContainingIgnoreCase(adorgid, documentnoPatterm)
				.get(cor.findByAdorgidAndSoChungTuContainingIgnoreCase(adorgid, documentnoPatterm).size() - 1);
//		return mRepository.findByAdorgidAndDocumentnoContainingIgnoreCase(adorgid, documentnoPatterm);

	}

	// POST
	@RequestMapping(method = RequestMethod.POST, value = "/updatecloseorder")
	public Order closeOrder(@RequestParam long order_id) {
		Order o = new Order();
		o = cor.findById(order_id);
		o.setDocStatus("CL");
		return cor.saveAndFlush(o);
	}

	@PostMapping
	@RequestMapping("create")
	public Order createNew(@RequestParam String documentno, @RequestParam long adorgid,
			@RequestParam long c_bpartner_id, @RequestParam long m_warehouse_id) {
		Order o1 = new Order();
		o1.setAd_client_id(1000003);
		o1.setAd_org_id((int) adorgid);
		o1.setC_order_uu(g.getUUID());
		o1.setDienGiai(null);
		o1.setDocStatus("CO"); // trạng thái hoàn thành CO , đóng CL
		o1.setDoctypeId(0);
		o1.setDoiTac((int) c_bpartner_id);
		o1.setId((long) g.getNextID("C_Order"));
		o1.setIsback("N");
		o1.setIsscanning("N");
		o1.setIsvanning("N");
		o1.setKhoHang((long) m_warehouse_id);
		o1.setCreated(g.getDate());
		o1.setUpdated(g.getDate());
		o1.setCreatedby(100);
		o1.setDocaction("CO");
		o1.setUpdatedby(100);
		o1.setDateacct(Timestamp.valueOf(g.getDate().toString().substring(0, 10) + " 00:00:00"));
		o1.setLoaiChungTu((long) 1000135);
		o1.setC_bpartner_location_id(1002085);
		o1.setC_currency_id(234);
		o1.setNgayDatHang(g.getDate());
		o1.setPaymentrule("P");
		o1.setC_paymentterm_id(105);
		o1.setInvoicerule("D");
		o1.setDeliveryrule("A");
		o1.setFreightcostrule("I");
		o1.setDeliveryviarule("P");
		o1.setPriorityrule("5");
		o1.setM_pricelist_id(1000003);
//		o1.set
		o1.setNgayDuKienGiaoHang(g.getDate());
		o1.setProcessed("N");
		o1.setSoChungTu(documentno);
		return cor.saveAndFlush(o1);
	}

	// Get KHXH
	@GetMapping
	@RequestMapping("/aaa")
	public Orderline abc(){
		return olr.getOne((long) 1358072);
	}
	public Long totalPricePayment(long c_order_id, String type) {
		List<Orderline> list = new ArrayList<Orderline>();
		long price = 0;
		System.out.println("C_order_id : " + c_order_id);
		

			for (Orderline mi : olr.findByOrderIDAndSoLuongGreaterThan(c_order_id,0)) {
				if (type.equals("V+")) {
					// KHNH
					try {
						price += mpp.findByProductid(mi.getSanPham()).get(0).getPricestd() * mi.getSoLuong();// giá mua
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println("Can't find a proructPricestd");
					}

				} else if (type.equals("C-")) {
					// KHXH
					try {
						price += mpp.findByProductid(mi.getSanPham()).get(0).getPricelist();// giá mua
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println("Can't find a proructPricelist");
					}

				}
			}
			return price;
	}

	@GetMapping
	@RequestMapping("getkhxh") // for Base Application
	public List<Order> listKHXH(@RequestParam long adorgid, @RequestParam long m_warehouse_id) {
		List<Order> list = new ArrayList<Order>();
		for (Order m : cor.findByKhoHangAndLoaiChungTuAndDocStatus(m_warehouse_id, 1000135, "DR")) {
			if (m.getDocStatus().equals("DR")) {
				m.setDienGiai("Nháp");
			} else if (m.getDocStatus().equals("CO")) {
				m.setDienGiai("Hoàn thành");
			} else if (m.getDocStatus().equals("CL")) {
				m.setDienGiai("Đóng");
			} else {
				m.setDienGiai(m.getDocStatus());
			}
			m.setUpdatedby(totalPricePayment(m.getId(), "C-"));
			list.add(m);
		}
		return list;
	}

	@GetMapping
	@RequestMapping("getkhxhbase1") // for Base Application
	public List<C_OrderInformation> listKHXH1(@RequestParam long adorgid, @RequestParam long m_warehouse_id) {
		String orgDes=aor.getDesOrg(adorgid);
		List<C_OrderInformation> list = new ArrayList<C_OrderInformation>();
		for (Order m : cor.findByKhoHangAndLoaiChungTuAndDocStatus(m_warehouse_id, 1000135, "CO")) {
			if (m.getDocStatus().equals("DR")) {
				m.setDienGiai("Nháp");
			} else if (m.getDocStatus().equals("CO")) {
				m.setDienGiai("Hoàn thành");
			} else if (m.getDocStatus().equals("CL")) {
				m.setDienGiai("Đóng");
			} else {
				m.setDienGiai(m.getDocStatus());
			}
			try {
				m.setUpdatedby(totalPricePayment(m.getId(), "C-"));
			} catch (Exception e) {
				m.setUpdatedby(0);
				// TODO: handle exception
			}
			

			C_OrderInformation c1 = new C_OrderInformation();
			c1.setC_bpartner_name(c_bPartnerRepository.findByCbpartnerid(m.getDoiTac()).getName().replaceAll("-"+orgDes, ""));
			c1.setC_order_id(m.getId());
			c1.setDateordered(m.getNgayDatHang().toString().substring(0, 10));
			c1.setDocumentno(m.getSoChungTu());
			c1.setPriceSales((double) m.getUpdatedby());
			list.add(c1);
		}
		return list;
	}
	@GetMapping
	@RequestMapping("findbydocumentnobase")
	public Order getByDocumentno1(@RequestParam long adorgid,@RequestParam String documentno) {
		try {
			
			return cor.findByAdorgidAndSoChungTuContainingIgnoreCase(adorgid, documentno).get(0);
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
		
	}
	@GetMapping
	@RequestMapping("findbydocumentnobase1")
	public Order getByDocumentno(@RequestParam long adorgid) {
		if(cor.getDocumentnoMax(adorgid).size()>0)
			return cor.getDocumentnoMax(adorgid).get(0);
		else {
			return null;
		}
	}
	
}
