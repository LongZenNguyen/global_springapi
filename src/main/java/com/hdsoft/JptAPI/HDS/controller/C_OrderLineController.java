package com.hdsoft.JptAPI.HDS.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hdsoft.JptAPI.HDS.Repositories.Ad_orgRepository;
import com.hdsoft.JptAPI.HDS.Repositories.m_inoutlineRepository;
import com.hdsoft.JptAPI.HDS.Repositories.m_productRepository;
import com.hdsoft.JptAPI.HDS.model.C_orderLineInformation;
import com.hdsoft.JptAPI.HDS.model.m_inoutline;
import com.hdsoft.JptAPI.Models.Orderline;
import com.hdsoft.JptAPI.Repositories.OrderlineRepository;

@RestController
@RequestMapping("/api/v1/c_orderline")
public class C_OrderLineController {
	@Autowired
	OrderlineRepository or;
	@Autowired
	m_productRepository mp;
	@Autowired
	m_inoutlineRepository mil1;
	@Autowired  
	Ad_orgRepository aor;
	GetIDUUDate g = new GetIDUUDate();
	//HDS App Base
	//xuat hang
	@PostMapping
	@RequestMapping("detail")
	public Orderline detail(
			@RequestParam long c_order_id,@RequestParam long m_product_id
			,@RequestParam long qtyentered,
			@RequestParam long movementqty,@RequestParam long line,@RequestParam long adorgid,@RequestParam long m_warehouse_id,@RequestParam long totalprice) {
		Orderline o1 = new Orderline();
		o1.setC_orderline_uu(g.getUUID());
		o1.setCarton((long) 0);
		o1.setClientId((long) 1000003);
		o1.setCtnpallet(0);
		o1.setCtnpallet(0);
		o1.setDvt( mp.getOne(m_product_id).getC_uom_id());
		o1.setId((long) g.getNextID("C_OrderLine"));
		o1.setInvoiceno(null);
		o1.setLot(null);
		o1.setOrderID(c_order_id);
		o1.setOrderno("");
		o1.setPalletno(totalprice);// giá của 1 sản phẩm
		o1.setQtypallet((double) 0);
		o1.setCreated(g.getDate());
		o1.setCreatedby((long) 100);
		o1.setUpdated(g.getDate());
		o1.setUpdatedby((long) 100);
		o1.setQtyslc((double)0);
		o1.setDateordered(Timestamp.valueOf(g.getDate().toString().substring(0, 10)+ " 00:00:00"));
		o1.setSanPham(m_product_id);
		o1.setSoLuong( qtyentered);
		o1.setSoLuongThucTe((double) 0);
		o1.setM_warehouse_id(m_warehouse_id);
		o1.setVendorprefix(null);
		o1.setC_currency_id((long) 234);
		o1.setAdorgid(adorgid);
		o1.setC_tax_id((long) 1000005);
		o1.setPriceentered((long) 12);
		o1.setLine(line);
		
		return or.saveAndFlush(o1);
	}
	//HDS Base App
	@GetMapping
	@RequestMapping("1")
	public Double abc() {
		return mil1.SumByOrderLineID(1002600);
	}
	
	@GetMapping
	@RequestMapping("detailkhxhbase")
	public List<C_orderLineInformation> detailKHXH(@RequestParam long c_order_id){
		List<C_orderLineInformation> listOrderlines= new ArrayList<C_orderLineInformation>();
		String orgDes="";
		try {
			orgDes=aor.getDesOrg(or.findByOrderID(c_order_id).get(0).getAdorgid());
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		for (Orderline o1 : or.findByOrderID(c_order_id)) {
			C_orderLineInformation c1 = new C_orderLineInformation();
			c1.setC_orderline_id(o1.getId());
			c1.setM_product_id(o1.getSanPham());
			c1.setQtyentered(o1.getSoLuong());
			
			try {
				c1.setQtyreceived(mil1.SumByOrderLineID(o1.getId()));
				System.out.println("orlineid: "+o1.getId()+" qtyget: "+c1.getQtyreceived());
			} catch (Exception e) {
				c1.setQtyreceived(0);
			}
			
			double confirmStatus;
			confirmStatus = o1.getSoLuong()-c1.getQtyreceived();
			c1.setQtythieu(confirmStatus);
			if(confirmStatus>0) {
				c1.setStatus("Thiếu "+confirmStatus);
			}else if (confirmStatus==0) {
				c1.setStatus("Đủ");
			}else {
				c1.setStatus("Thừa "+(confirmStatus*(-1)));
			}
			c1.setProductvalue(mp.getOne(o1.getSanPham()).getValue().replaceAll("-"+orgDes, ""));
			listOrderlines.add(c1);
			
		}
	
		return listOrderlines;
	}
}
