package com.hdsoft.JptAPI.HDS.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.SortOrder;
import javax.swing.plaf.MenuItemUI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hdsoft.JptAPI.HDS.model.m_inout;
import com.hdsoft.JptAPI.HDS.model.m_inoutline;
import com.hdsoft.JptAPI.HDS.Repositories.*;
@RestController
@RequestMapping("api/v1/lm_inout")
public class m_inoutController {
	GetIDUUDate g = new GetIDUUDate();
		@Autowired
		m_inoutRepository mRepository;
		@Autowired
		m_inoutlineRepository mlr;
		@Autowired
		M_ProductPriceRepository mpp;
		//GET
		@GetMapping()
		public List<m_inout> findAll(){
			return mRepository.findAll();
		}
		@GetMapping
		@RequestMapping("/documentno")
		public m_inout maxDocumentNo(@RequestParam String documentno) {
			return mRepository.findByDocumentnoStartingWith(documentno).get(mRepository.findByDocumentnoStartingWith(documentno).size()-1);
		}
		
		
		//POST
		@RequestMapping(method = RequestMethod.POST,value="/nhapkho")
		public m_inout insertNewRecord(@RequestParam String documentNo,
				@RequestParam long c_order_id) {
			m_inout miu = new m_inout();
			miu.setAd_client_id(1000014);
			miu.setId(g.getNextID("M_InOut"));
			miu.setAd_org_id(1000039);
			miu.setC_bpartner_id(1003545);
			miu.setC_doctype_id(1000856); 
			miu.setC_invoice_id(null);
			miu.setC_order_id(c_order_id);
			miu.setCreated(g.getDate());
			miu.setM_warehouse_id(1000075);
			miu.setDocstatus("DR");
			miu.setDocumentno(documentNo);
			miu.setCreatedby((long) 1003545);
			miu.setUpdatedby(1003545);
			miu.setDocaction("CL");
			miu.setMovementtype("V+");
			miu.setMovementdate(g.getDate());
			miu.setDateacct(g.getDate());
			miu.setDropship_location_id(null);
			miu.setM_inout_uu(g.getUUID());
			miu.setDeliveryrule('A');
			miu.setFreightcostrule('I');
			miu.setDeliveryviarule('P');
			miu.setPriorityrule('5');
			miu.setC_bpartner_location_id((long) 1002013);
			System.out.println(miu.toString());
			return mRepository.saveAndFlush(miu);
		}
		 
		//GET
		@GetMapping
		@RequestMapping("getmaxdocumentbyorg")
		public String getmaxbyorg(@RequestParam long adclientid,@RequestParam String documentnoPatterm) {
//			return mRepository.
			String maxIDFocusDocuString=mRepository.documentnoMaxID(adclientid,documentnoPatterm+"%");
			
			System.out.println("ax: "+maxIDFocusDocuString);
			if (maxIDFocusDocuString==null) {
				return documentnoPatterm.replaceAll("-", "")+"-1";
			}else {
				System.out.println("length: "+maxIDFocusDocuString.length());
				int index;
				for (int i = 0; i < maxIDFocusDocuString.length(); i++) {
					if (maxIDFocusDocuString.charAt(i) == '-') {
						index = i;
						break;
					}
				}
				System.out.println("num: "+maxIDFocusDocuString.substring(5,maxIDFocusDocuString.length()));
				int LastNumber = Integer.parseInt(maxIDFocusDocuString.substring(5,maxIDFocusDocuString.length()))+1;
				String newDocument= "NHGL-"+LastNumber;
				//System.out.println("index: "+index);
				
						return newDocument;
			}
			
//			return mRepository.findByAdorgidAndDocumentnoContainingIgnoreCase(adorgid, documentnoPatterm);
			
		}
		
		@RequestMapping(method = RequestMethod.POST,value="/create")
		public m_inout createnew(@RequestParam String documentNo,@RequestParam long adorgid,@RequestParam long c_bpartner_id
				,@RequestParam long warehouseid) {
			m_inout miu = new m_inout();
			miu.setAd_client_id(1000003);
			miu.setId(g.getNextID("M_InOut"));
			miu.setAd_org_id(adorgid);
			miu.setC_bpartner_id(c_bpartner_id);
			miu.setC_doctype_id(1000117); 
			miu.setC_invoice_id(null);
			miu.setC_order_id(null);
			miu.setCreated(g.getDate());
			miu.setM_warehouse_id(warehouseid);
			miu.setDocstatus("DR");
			miu.setDocumentno(documentNo);
			miu.setCreatedby((long) 100);
			miu.setUpdatedby(100);
			miu.setUpdated(g.getDate());
			miu.setDocaction("CL");
			miu.setMovementtype("V+");
			miu.setMovementdate(g.getDate());
			miu.setDateacct(g.getDate());
			miu.setDropship_location_id(null);
			miu.setM_inout_uu(g.getUUID());
			miu.setDeliveryrule('A');
			miu.setFreightcostrule('I');
			miu.setDeliveryviarule('P');
			miu.setPriorityrule('5');
			//get c_bpartner_location_id từ c_order: 
			miu.setC_bpartner_location_id((long) 1002085);
			System.out.println(miu.toString());
			return mRepository.saveAndFlush(miu);
		}
		
		//HDS Base APP
		//GET
		public long totalPricePayment(long m_inout_id,String type){
			List<m_inoutline> list = new ArrayList<m_inoutline>();
			long price=0;
			for (m_inoutline mi : mlr.findByMinoutid(m_inout_id)) {
				if (type.equals("V+")) {
					//KHNH
					try {
						price+=mpp.findByProductid(mi.getM_product_id()).get(0).getPricestd()*mi.getQtyentered();//giá mua
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println("Can't find a proructPricestd");
					}
					
				}else if(type.equals("C-")) {
					//KHXH
					try {
						price+=mpp.findByProductid(mi.getM_product_id()).get(0).getPricelist();//giá mua
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println("Can't find a proructPricelist");
					}
					
				}
			}
			return price;
		}
		
		@GetMapping
		@RequestMapping("getkhnh")
		public List<m_inout> listKHNH(@RequestParam long adorgid){
//			System.out.println("Size KHNH: "+mRepository.findTop100ByMovementtypeAndAdorgid("V+", adorgid).size());
			List<m_inout> list = new ArrayList<m_inout>();
			for (m_inout m : mRepository.findTop100ByMovementtypeAndAdorgid("V+", adorgid)) {
				if(m.getDocstatus().equals("DR")) {
					m.setDescription("Nháp");
				}else if(m.getDocstatus().equals("CO")) {
					m.setDescription("Hoàn thành");
				}else if(m.getDocstatus().equals("CL")) {
					m.setDescription("Đóng");
				}else {
					m.setDescription(m.getDocstatus());
				}
				m.setC_invoice_id(totalPricePayment(m.getId(), "V+"));
				list.add(m);
			}
			return list;
		}
		@GetMapping
		@RequestMapping("getkhxh")
		public List<m_inout> listKHXH(@RequestParam long adorgid){
			List<m_inout> list = new ArrayList<m_inout>();
			for (m_inout m : mRepository.findTop100ByMovementtypeAndAdorgid("C-", adorgid)) {
				if(m.getDocstatus().equals("DR")) {
					m.setDescription("Nháp");
				}else if(m.getDocstatus().equals("CO")) {
					m.setDescription("Hoàn thành");
				}else if(m.getDocstatus().equals("CL")) {
					m.setDescription("Đóng");
				}else {
					m.setDescription(m.getDocstatus());
				}
				m.setC_invoice_id(totalPricePayment(m.getId(), "C-"));
				list.add(m);
			}
			return list;
		}
		
		
		
		
		
		
		
		
		
		
		/////////////////////////////////
		@RequestMapping(method = RequestMethod.POST,value="/createxk")
		public m_inout createnewXK(@RequestParam String documentNo,@RequestParam long adorgid,@RequestParam long c_bpartner_id
				,@RequestParam long warehouseid) {
			m_inout miu = new m_inout();
			miu.setAd_client_id(1000003);
			miu.setId(g.getNextID("M_InOut"));
			miu.setAd_org_id(adorgid);
			miu.setC_bpartner_id(c_bpartner_id);
			miu.setC_doctype_id(1000117); 
			miu.setC_invoice_id(null);
			miu.setC_order_id(null);
			miu.setCreated(g.getDate());
			miu.setM_warehouse_id(warehouseid);
			miu.setDocstatus("DR");
			miu.setDocumentno(documentNo);
			miu.setCreatedby((long) 100);
			miu.setUpdatedby(100);
			miu.setUpdated(g.getDate());
			miu.setDocaction("CL");
			miu.setMovementtype("C-");
			miu.setMovementdate(g.getDate());
			miu.setDateacct(g.getDate());
			miu.setDropship_location_id(null);
			miu.setM_inout_uu(g.getUUID());
			miu.setDeliveryrule('A');
			miu.setFreightcostrule('I');
			miu.setDeliveryviarule('P');
			miu.setPriorityrule('5');
			//get c_bpartner_location_id từ c_order: 
			miu.setC_bpartner_location_id((long) 1002085);
			System.out.println(miu.toString());
			return mRepository.saveAndFlush(miu);
		}
		@RequestMapping(method = RequestMethod.POST,value="/createbase")
		public m_inout createnew1Base(@RequestParam String documentNo,@RequestParam long adorgid,@RequestParam long c_bpartner_id
				,@RequestParam long warehouseid,@RequestParam long c_order_id) {
			m_inout miu = new m_inout();
			miu.setAd_client_id(1000003);
			miu.setId(g.getNextID("M_InOut"));
			miu.setAd_org_id(adorgid);
			miu.setC_bpartner_id(c_bpartner_id);
			miu.setC_doctype_id(1000117); 
			miu.setC_invoice_id(null);
			miu.setC_order_id(c_order_id);
			miu.setCreated(g.getDate());
			miu.setM_warehouse_id(warehouseid);
			miu.setDocstatus("DR");
			miu.setDocumentno(documentNo);
			miu.setCreatedby((long) 100);
			miu.setUpdatedby(100);
			miu.setUpdated(g.getDate());
			miu.setDocaction("CL");
			miu.setMovementtype("C-");
			miu.setMovementdate(g.getDate());
			miu.setDateacct(g.getDate());
			miu.setDropship_location_id(null);
			miu.setM_inout_uu(g.getUUID());
			miu.setDeliveryrule('A');
			miu.setFreightcostrule('I');
			miu.setDeliveryviarule('P');
			miu.setPriorityrule('5');
			//get c_bpartner_location_id từ c_order: 
			miu.setC_bpartner_location_id((long) 1002085);
			System.out.println(miu.toString());
			return mRepository.saveAndFlush(miu);
		}
		
		@PutMapping
		@RequestMapping("closekhnh")
		public m_inout closeKH(@RequestParam long m_inout_id) {
			m_inout m = new m_inout();
			m = mRepository.getOne(m_inout_id);
			m.setDocstatus("CO");
			return mRepository.saveAndFlush(m);
		}
		 
		
}
