package com.hdsoft.JptAPI.HDS.controller;

import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hdsoft.JptAPI.HDS.Repositories.M_MovementRepository;
import com.hdsoft.JptAPI.HDS.model.M_MovementInfomation;
import com.hdsoft.JptAPI.Models.MMovement;

@RestController
@RequestMapping("api/v1/movementinfo")
public class M_MovementInfomationController {
	@Autowired
	M_MovementRepository mmr;
	//HDS Base App
	@GetMapping
	@RequestMapping("info")
	public M_MovementInfomation getMovement(@RequestParam long movementID) {
		M_MovementInfomation  m = new M_MovementInfomation();
		MMovement m1 = new MMovement();
		m1 = mmr.findByMovementID(movementID);
		m.setDocaction(m1.getDocaction());
		m.setDocstatus(m1.getDocstatus());
		m.setStatus(m1.getStatus());
		if(m1.getDocstatus().equals("CL")) {
			m.setDocstatusconvert("Đóng");
		}else if (m1.getDocstatus().equals("DR")) {
			m.setDocstatusconvert("Nháp");
		}else if(m1.getDocstatus().equals("IN")) {
			m.setDocstatusconvert("Không hợp lệ");
		}else if(m1.getDocstatus().equals("CO")) {
			m.setDocstatusconvert("Đã hoàn thành");
		}else {
			m.setDocstatusconvert(m1.getDocstatus());
		}
		m.setDocumentno(m1.getDocumentno());
		m.setMovementdate(m1.getMovementDate());
		m.setMovementID(m1.getMovementID());
		return m;
	}
	
	@GetMapping
	@RequestMapping("all")
	public List<M_MovementInfomation> findAll(@RequestParam long adorgid){
		List<M_MovementInfomation> list = new ArrayList<M_MovementInfomation>();
		for (MMovement m1 : mmr.findTop100ByAdorgidOrderByMovementIDDesc(adorgid)) {
			M_MovementInfomation  m = new M_MovementInfomation();
			m.setDocaction(m1.getDocaction());
			m.setDocstatus(m1.getDocstatus());
			m.setStatus(m1.getStatus());
			if(m1.getDocstatus().equals("CL")) {
				m.setDocstatusconvert("Đóng");
			}else if (m1.getDocstatus().equals("DR")) {
				m.setDocstatusconvert("Nháp");
			}else if(m1.getDocstatus().equals("IN")) {
				m.setDocstatusconvert("Không hợp lệ");
			}else if(m1.getDocstatus().equals("CO")) {
				m.setDocstatusconvert("Đã hoàn thành");
			}else {
				m.setDocstatusconvert(m1.getDocstatus());
			}
			m.setDocumentno(m1.getDocumentno());
			m.setMovementdate(m1.getMovementDate());
			m.setMovementID(m1.getMovementID());
			list.add(m);
		}
//		list.sort();
		System.out.println("Size list: "+list.size());
		return list;
	}
	
	//==============================================================
	
	
}
