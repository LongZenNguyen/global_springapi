package com.hdsoft.JptAPI.HDS.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.hdsoft.JptAPI.HDS.Repositories.M_MovementRepository;
import com.hdsoft.JptAPI.HDS.Repositories.m_locatorRepository;
import com.hdsoft.JptAPI.HDS.Repositories.m_storageonhandRepository;
import com.hdsoft.JptAPI.HDS.Repositories.m_transactionRepository;
import com.hdsoft.JptAPI.HDS.model.m_storageonehand;
import com.hdsoft.JptAPI.HDS.model.m_transaction;

@RestController
@RequestMapping("api/v1/movementprocess")
public class ProcessController {
	@Autowired
	m_storageonhandRepository ms;
	@Autowired
	m_transactionRepository mt;
	@Autowired
	M_MovementRepository mm;
	@Autowired
	m_locatorRepository mlr;
	GetIDUUDate g = new GetIDUUDate();
	// HDS Base App

//	@PostMapping
//	@RequestMapping("createtran")
	public String createTransaction(long m_product_id, long locatorid, long qty, long adorgid, String movementtype) {
		m_transaction m_transaction = new m_transaction();
		m_transaction.setAd_client_id((long) 1000003);
		m_transaction.setAd_org_id(adorgid);
		m_transaction.setCreated(g.getDate());
		m_transaction.setCreatedby((long) 100);
		m_transaction.setId(g.getNextID("M_Transaction"));
		m_transaction.setM_attributesetinstance_id((long) 0);
		m_transaction.setM_inoutline_id(null);
		m_transaction.setM_locator_id(locatorid);
		m_transaction.setM_product_id(m_product_id);
		m_transaction.setM_transaction_uu(g.getUUID());
		m_transaction.setMovementdate(g.getDate());
		m_transaction.setMovementqty(qty);
		m_transaction.setMovementtype(movementtype);
		m_transaction.setUpdated(g.getDate());
		m_transaction.setUpdatedby((long) 100);
		mt.save(m_transaction);
		return "Sucessful!";
	}

	@GetMapping
	@RequestMapping("abc")
	public int a() {
		List<m_storageonehand> list = new ArrayList<m_storageonehand>();
		list.add(ms.findByLocatoridAndProductid(1033883, 1237947));
		if (ms.findByLocatoridAndProductid(1033883, 1237947) == null) {
			System.out.println("Null");
		}
		return list.size();
	}

//	@PostMapping
//	@RequestMapping("processqty")
	public String processStorage(long adorgid, long locatorid, long locatortoid, long productid, long qty) {
		m_storageonehand storageonehand = new m_storageonehand();
		m_storageonehand s1 = new m_storageonehand();// chứa dữ liệu tồn cũ của sp
		long oldqty;
		if (ms.findByLocatoridAndProductid(locatortoid, productid) != null) {
			storageonehand = ms.findByLocatoridAndProductid(locatortoid, productid);
			oldqty = storageonehand.getQtyonhand();
			storageonehand.setQtyonhand((oldqty + qty));
			System.out.println("before  save");
			ms.save(storageonehand);
			System.out.println("After save");
			System.out.println("Cộng số liệu thành công!");

		} else {
			// Không có record
			// Tạo record và cộng slg vị trí mới
			storageonehand.setAd_org_id(adorgid);
			storageonehand.setAsiid((long) 0);
			storageonehand.setCreated(g.getDate());
			storageonehand.setCreatedby(100);
			storageonehand.setDatematerialpolicy(Timestamp.valueOf(java.time.LocalDate.now() + " 00:00:00"));
			storageonehand.setId(1000003);
			storageonehand.setLocatorid(locatortoid);
			storageonehand.setM_storageonhand_uu(g.getUUID());
			storageonehand.setProductid(productid);
			storageonehand.setQtyonhand(qty);
			storageonehand.setUpdated(g.getDate());
			storageonehand.setUpdatedby(100);
			ms.save(storageonehand);
			System.out.println("Tạo record thành công!");
		} // trừ slg từ vị trí cũ
		s1 = ms.findByLocatoridAndProductid(locatorid, productid);
		oldqty = s1.getQtyonhand();
		s1.setQtyonhand(oldqty - qty);
		ms.save(s1);
		System.out.println("Trừ số liệu thành công!");
		return "Sucessful!";
	}

	@PostMapping
	@RequestMapping("/process")
	public String processFunc(@RequestParam long locatorid, @RequestParam long locatortoid,
			@RequestParam long productid, @RequestParam long adorgid, @RequestParam long qty) {
//		
		try {
			processStorage(adorgid, locatorid, locatortoid, productid, qty);
			System.out.println("Xu ly so lieu xong");
			createTransaction(productid, locatorid, (qty * (-1)), adorgid, "M-");
			System.out.println("tran 1");
			createTransaction(productid, locatortoid, qty, adorgid, "M+");
			System.out.println("tran 2");
		} catch (Exception e) {
			System.out.println("Rollback!");
			System.out.println("error:" + e);
		}
		return "Successfully!";
	}

//	NH
	@PostMapping
	@RequestMapping("processqty")
	public String processStorageNH(long adorgid,  long locatortoid, long productid, long qty) {
		m_storageonehand storageonehand = new m_storageonehand();
		m_storageonehand s1 = new m_storageonehand();// chứa dữ liệu tồn cũ của sp
		long oldqty;
//		System.out.println(s1.getLocatorid()+"");
		// check vị trí tới đã có chứa sp và slg hay chưa để lựa chọn update hay insert
		if (ms.findByLocatoridAndProductid(locatortoid, productid) != null) {
			// đã có record
			// cộng slg vào vị trí mới
			storageonehand = ms.findByLocatoridAndProductid(locatortoid, productid);
			oldqty = storageonehand.getQtyonhand();
			storageonehand.setQtyonhand((oldqty + qty));
			System.out.println("before  save");
			ms.save(storageonehand);
			System.out.println("After save");
			System.out.println("Cộng số liệu thành công!");

		} else {
			// Không có record
			// Tạo record và cộng slg vị trí mới
			storageonehand.setAd_org_id(adorgid);
			storageonehand.setAsiid((long) 0);
			storageonehand.setCreated(g.getDate());
			storageonehand.setCreatedby(100);
			storageonehand.setDatematerialpolicy(Timestamp.valueOf(java.time.LocalDate.now() + " 00:00:00"));
			storageonehand.setId(1000003);
			storageonehand.setLocatorid(locatortoid);
			storageonehand.setM_storageonhand_uu(g.getUUID());
			storageonehand.setProductid(productid);
			storageonehand.setQtyonhand(qty);
			storageonehand.setUpdated(g.getDate());
			storageonehand.setUpdatedby(100);
			ms.save(storageonehand);
			System.out.println("Tạo record thành công!");
		} 
		return "Sucessful!";
	}

	//Process NH
	public String processStoragenh(long adorgid, long locatorid, long locatortoid, long productid, long qty) {
		m_storageonehand storageonehand = new m_storageonehand();
		m_storageonehand s1 = new m_storageonehand();// chứa dữ liệu tồn cũ của sp
		long oldqty;
		if (ms.findByLocatoridAndProductid(locatortoid, productid) != null) {
			storageonehand = ms.findByLocatoridAndProductid(locatortoid, productid);
			oldqty = storageonehand.getQtyonhand();
			storageonehand.setQtyonhand((oldqty + qty));
			System.out.println("before  save");
			ms.save(storageonehand);
			System.out.println("After save");
			System.out.println("Cộng số liệu thành công!");

		} else {
			// Không có record
			// Tạo record và cộng slg vị trí mới
			storageonehand.setAd_org_id(adorgid);
			storageonehand.setAsiid((long) 0);
			storageonehand.setCreated(g.getDate());
			storageonehand.setCreatedby(100);
			storageonehand.setDatematerialpolicy(Timestamp.valueOf(java.time.LocalDate.now() + " 00:00:00"));
			storageonehand.setId(1000003);
			storageonehand.setLocatorid(locatortoid);
			storageonehand.setM_storageonhand_uu(g.getUUID());
			storageonehand.setProductid(productid);
			storageonehand.setQtyonhand(qty);
			storageonehand.setUpdated(g.getDate());
			storageonehand.setUpdatedby(100);
			ms.save(storageonehand);
			System.out.println("Tạo record thành công!");
		} // trừ slg từ vị trí cũ
//		s1 = ms.findByLocatoridAndProductid(locatorid, productid);
//		oldqty = s1.getQtyonhand();
//		s1.setQtyonhand(oldqty - qty);
//		ms.save(s1);
//		System.out.println("Trừ số liệu thành công!");
		return "Sucessful!";
	}

	@PostMapping
	@RequestMapping("/processnh")
	public String processFuncNhapHang(  @RequestParam long locatortoid,
			@RequestParam long productid, @RequestParam long adorgid, @RequestParam long qty) {
//		
		try {
			/*long NHLocator =mlr.findByAdorgidAndValue(adorgid, "Nhận hàng").getId();
			System.out.println("ID NH: "+NHLocator);
			System.out.println("1");*/
			processStoragenh(adorgid,0,  locatortoid, productid, qty);
			System.out.println("Xu ly so lieu xong");
			//createTransaction(productid, 1033888, qty*(-1), adorgid, "M-");
			//System.out.println("tran 1");
			createTransaction(productid, locatortoid, qty, adorgid, "V+");
			System.out.println("tran 2");
		} catch (Exception e) {
			System.out.println("error:" + e);
		}
		return "Successfully!";
	}
	public String processStorageXK(long adorgid, long locatorid, long productid, long qty) {
		m_storageonehand storageonehand = new m_storageonehand();
		m_storageonehand s1 = new m_storageonehand();// chứa dữ liệu tồn cũ của sp
		long oldqty;
		System.out.println("locatorid: "+locatorid+"productid: "+productid);
		System.out.println("=======1");
		s1 = ms.findByLocatoridAndProductid(locatorid, productid);
		System.out.println("=======2");
		oldqty = s1.getQtyonhand();
		System.out.println("========3");
		System.out.println("old: "+oldqty+" qtysub: "+qty);
		s1.setQtyonhand(oldqty - qty);
		System.out.println("======4");
		
		ms.save(s1);
		System.out.println("======5");
		System.out.println("Trừ số liệu thành công! New qty: "+s1.getQtyonhand());
		return "Sucessful!";
	}
	@PostMapping
	@RequestMapping("/processxh")
	public String processFuncXuatHang(@RequestParam long locatorid,@RequestParam long productid, @RequestParam long adorgid, @RequestParam long qty) {
		try {
			processStorageXK(adorgid,locatorid, productid, qty);
			System.out.println("Xu ly so lieu xong");
			createTransaction(productid, locatorid, qty*(-1), adorgid, "C-");
		} catch (Exception e) {
			System.out.println("error:" + e);
		}
		return "Successfully!";
	}

}
