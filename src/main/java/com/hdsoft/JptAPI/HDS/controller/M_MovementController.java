package com.hdsoft.JptAPI.HDS.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hdsoft.JptAPI.HDS.Repositories.M_MovementRepository;
import com.hdsoft.JptAPI.Models.MMovement;

@RestController
@RequestMapping("api/v1/m_movement")
public class M_MovementController {
	@Autowired
	M_MovementRepository mRepository;
	//HDS Base
	//-----------GET-------------------
	
	
	
	//------------PUT-----------------
	@PutMapping
	@RequestMapping("complete")
	public MMovement htMovement(@RequestParam long movementid) {
		MMovement mMovement = new MMovement();
		mMovement =  mRepository.findByMovementID(movementid);
		mMovement.setDocstatus("CO");
		return mRepository.saveAndFlush(mMovement);
	}
	
}
