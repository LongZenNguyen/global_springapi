package com.hdsoft.JptAPI.HDS.controller;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.sym.Name;
import com.hdsoft.JptAPI.HDS.Repositories.Ad_orgRepository;
import com.hdsoft.JptAPI.HDS.Repositories.m_locatorRepository;
import com.hdsoft.JptAPI.HDS.model.Ad_OrgModel;
import com.hdsoft.JptAPI.HDS.model.m_locator;

@RestController
@RequestMapping("api/v1/adorg")
public class Ad_orgController {
	GetIDUUDate g = new GetIDUUDate();
	@Autowired
	Ad_orgRepository ar;
	@Autowired
	m_locatorRepository ml1;
	//GET
	@GetMapping
	public List<Ad_OrgModel> showAll(){
		return ar.findAll();
	}
	@GetMapping
	@RequestMapping("timkiem")
	public Ad_OrgModel find(@RequestParam String name, @RequestParam String value){
		return ar.findByNameOrValue(name, value);
	}
	@GetMapping
	@RequestMapping("gencode")
	public String genCode() {
		String  code= "HDS";
	
		Random random = new Random();
		while((""+code).length() < 8) {
			code += random.nextInt(9);
		}
		
		return code;
	}
	//POST
	@PostMapping
	@RequestMapping("/taochinhanh")
	public Ad_OrgModel TaoMoiToChuc(@RequestParam String name,@RequestParam String value) {
		Ad_OrgModel ad_OrgModel = new Ad_OrgModel();
		ad_OrgModel.setAd_org_id(g.getNextID("AD_Org"));
		ad_OrgModel.setAd_org_uu(g.getUUID());
		ad_OrgModel.setAdclientid(1000003);
		ad_OrgModel.setCreated(g.getDate());
		ad_OrgModel.setCreatedby(100);
		ad_OrgModel.setUpdated(g.getDate());
		ad_OrgModel.setUpdatedby(100);
		ad_OrgModel.setIssummary("N");
		ad_OrgModel.setName(name);
		ad_OrgModel.setValue(value);
		String description;
		description = name.replaceAll("Công ty", "");
		description = description.replaceAll("cty", "");
		String desLastString;
		desLastString = description.charAt(0)+"";
		for (int i = 1; i < description.length(); i++) {
			if ((""+description.charAt(i)).equals(" ")) {
				desLastString = desLastString+description.charAt(i+1);
			}
		}
		ad_OrgModel.setDescription(desLastString);
		if (ar.countLike(desLastString)>0) {
			String newDeslast = ar.getDescriptionMaxID(desLastString);
			int a = newDeslast.indexOf("-");
			String getNumberString = newDeslast.substring(a, newDeslast.length());
			ad_OrgModel.setDescription(desLastString+"-"+((Integer.parseInt(getNumberString)+1)+""));
		}
		return ar.saveAndFlush(ad_OrgModel);
	}
	
	
}
