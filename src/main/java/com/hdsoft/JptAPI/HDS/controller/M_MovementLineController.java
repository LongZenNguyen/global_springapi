package com.hdsoft.JptAPI.HDS.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hdsoft.JptAPI.Models.MMovementLine;
import com.hdsoft.JptAPI.Repositories.MMovementLineRepository;
@RestController
@RequestMapping("/api/v2/movementline")
public class M_MovementLineController {
	
	@Autowired
	private MMovementLineRepository movementLineRepository;
	GetIDUUDate g = new GetIDUUDate();
	
	
	
	@RequestMapping(method = RequestMethod.POST,value="/create1")
	public MMovementLine createne(@RequestParam long locator,
			@RequestParam long locatorto,@RequestParam long movementqty,
			@RequestParam long movementid
			,@RequestParam long productid,@RequestParam long adorgid) {
		MMovementLine m = new MMovementLine();
		m.setAttributeSetId(null);//1354623
		m.setCurrentLocatorId(locator);
		m.setLocatorToId(locatorto);
		m.setMovementId(movementid);
		m.setMovementLineId((long)g.getNextID("M_MovementLine"));
		m.setProductId(productid);
		m.setAd_client_id((long)1000003);
		m.setAd_org_id((long)adorgid);
		m.setCreatedby((long)100);
		m.setUpdatedby((long)100);
		m.setM_attributesetinstanceto_id(null);
		m.setCreated(g.getDate());
		m.setUpdated(g.getDate());
		m.setC_uom_id((long)100);
		m.setQuantity(movementqty);
		return movementLineRepository.saveAndFlush(m);
	}
}
