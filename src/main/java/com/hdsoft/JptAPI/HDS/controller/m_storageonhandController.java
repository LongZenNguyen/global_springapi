package com.hdsoft.JptAPI.HDS.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hdsoft.ConnectDB;
import com.hdsoft.JptAPI.HDS.Repositories.m_attributesetinstanceRepository;
import com.hdsoft.JptAPI.HDS.Repositories.m_productRepository;
import com.hdsoft.JptAPI.HDS.Repositories.m_storageonhandRepository;
import com.hdsoft.JptAPI.HDS.model.m_attributesetinstance;
import com.hdsoft.JptAPI.HDS.model.m_storageonehand;

@RestController
@RequestMapping("api/v1/storageonhand")
public class m_storageonhandController {
	GetIDUUDate g = new GetIDUUDate();
	@Autowired
	m_storageonhandRepository ms;
	@Autowired
	m_productRepository mp;
	@Autowired
	m_attributesetinstanceRepository ma;

	// GET
	@GetMapping
	@RequestMapping("/find")
	public m_storageonehand findByAsiidAndByLocatoridAndByProductid(@RequestParam long asiid,
			@RequestParam long locatorid, @RequestParam long productid) {
		return ms.findByAsiidAndLocatoridAndProductidAndAdclientid(asiid, locatorid, productid, 1000014);
	}

	// Vào kalipso và sử dụng tick replace trong json import table để lấy distinct
	// asiid
	@GetMapping
	@RequestMapping("/listasi")
	public List<m_storageonehand> findDistinctByAsiidAndId(@RequestParam long productid, @RequestParam long id) {
		return ms.findDistinctByProductidAndAdclientid(productid, id);
	}

	// lấy tất cả record để coi product đang trong những pallet nào
	@GetMapping
	@RequestMapping("/findbyproduct")
	public List<m_storageonehand> findByProduct(@RequestParam long productid) {
		return ms.findByProductid(productid);
	}

	// Lấy tất cả product đang có trong pallet id có sẵn
	@GetMapping
	@RequestMapping("/findbypallet")
	public List<m_storageonehand> findByPallet(@RequestParam long asiid, @RequestParam long locatorid) {
		System.out.println("Size Product on Pallet: ");
		return ms.findByAsiidAndAdclientidAndLocatorid(asiid, (long) 1000014, locatorid);
	}

	@GetMapping
	@RequestMapping("/findbypalletno")
	public List<m_storageonehand> findByPalletno(@RequestParam String lot, @RequestParam long locatorid) {
		System.out.println("Size Product on Pallet: ");
		List<Long> listAsi = new ArrayList<Long>();
		for (m_attributesetinstance m1 : ma.findByLotAndAdclient(lot, 1000014)) {
			listAsi.add(m1.getId());
		}
		List<m_storageonehand> m = new ArrayList<m_storageonehand>();
		m = ms.findByAdclientidAndLocatoridAndAsiidIn((long) 1000014, locatorid, listAsi);
		for (m_storageonehand m_storageonehand : m) {
			System.out.println("QtyOnHan: " + m_storageonehand.getQtyonhand());
			if (m_storageonehand.getQtyonhand() == 0) {
				System.out.println("Remove");
				m.remove(m_storageonehand);
			}
		}
		return m;
	}

	@GetMapping
	@RequestMapping("/findbylocator")
	public List<m_storageonehand> findbyLocatorID(@RequestParam long locatorid) {
		System.out.println("Find by locator ID");
		return ms.findByLocatorid(locatorid);
	}

	@GetMapping
	@RequestMapping("/findbylocatorproductasi")
	public m_storageonehand findByLocatoridAndProductidAndAsiid(@RequestParam long locatorid,
			@RequestParam long productid, @RequestParam long asiid) {
		return ms.findByLocatoridAndProductidAndAsiid(locatorid, productid, asiid);
	}

	// lấy dữ liệu tồn kho
	@GetMapping
	@RequestMapping("/tonkho")
	public List<m_storageonehand> tonkho() {
		return ms.findByAdclientidAndQtyonhandGreaterThan(1000000, 0);
	}

	// Lấy tồn theo vị trí và client\
	@GetMapping
	@RequestMapping("laytonkho")
	public List<m_storageonehand> laytonkho(@RequestParam long locatorid) {
		return ms.findByAdclientidAndLocatoridAndQtyonhandGreaterThan(1000000, locatorid, 0);
	}

	// POST
	@PostMapping
	@RequestMapping("insert")
	public m_storageonehand createnewStorageonhand(@RequestParam long locatorid, @RequestParam long productid,
			@RequestParam long asiid, @RequestParam long qty) {
		m_storageonehand m = new m_storageonehand();
		m.setId(1000014); // ad_client_id
		m.setCreated(g.getDate());
		m.setCreatedby(1000080);
		m.setAd_org_id(1000039);
		m.setUpdated(g.getDate());
		m.setUpdatedby(1000080);
		m.setAsiid(asiid);
		m.setLocatorid(locatorid);
		m.setM_storageonhand_uu(g.getUUID());
		m.setProductid(productid);
		m.setQtyonhand(qty);
		m.setDatematerialpolicy(Timestamp.valueOf(java.time.LocalDate.now() + " 00:00:00"));
		System.out.println("Giờ Convert: " + m.getDatematerialpolicy());
		return ms.saveAndFlush(m);
	}

	// PUT
	// Cộng trừ số liệu trong m_storageonhand theo locatorid,productid,asiid
	@PutMapping("update")
	public void UpdateQtyInStorage2(@RequestParam long locatorid, @RequestParam long productid,
			@RequestParam long asiid, @RequestParam long qty, @RequestParam String status) {
		// Tìm record
		System.out.println("Get old Data");
		m_storageonehand mst = new m_storageonehand();
		mst = ms.findByLocatoridAndProductidAndAsiid(locatorid, productid, asiid);
		long newqty = 0;
		// tìm được m_storageonhand thỏa mãn param truyền vô
		if (status.equalsIgnoreCase("add")) {
			newqty = mst.getQtyonhand() + qty;
			System.out.println("Cộng số liệu: " + newqty);
			mst.setQtyonhand(mst.getQtyonhand() + qty);
		} else {
			System.out.println("Trừ số liệu");
			mst.setQtyonhand(mst.getQtyonhand() - qty);
		}

		System.out.println("Qty after process: " + mst.getQtyonhand());

		// Xử lý số liệu
		String query = "update m_storageonhand set qtyonhand= " + newqty + " where m_locator_id= " + locatorid
				+ " AND m_product_id = " + productid + " and m_attributesetinstance_id = " + asiid
				+ " and ad_client_id = 1000014";
		System.out.println(query);
		try {
			System.out.println("Done");
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
		}
		ConnectDB connectDB = new ConnectDB();
		Connection con = null;
		try {
			con = connectDB.conHD();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
//		try {
//			System.out.println("set Drive");
//			con = DriverManager.getConnection("jdbc:postgresql://210.245.84.28:5432/id41HD", "adempiere",
//					"hdsofterppassword@123$");
//		} catch (SQLException e) {
//		}
		Statement sta = null;
		try {
			System.out.println("Create statement");
			sta = con.createStatement();
		} catch (SQLException e) {
		}
		try {
			System.out.println("execute query");
			sta.executeQuery(query);
		} catch (SQLException e) {
		}
		// xóa record có slg = 0

		System.out.println("Done");
	}

	// HDS Base
	// GET
	@GetMapping
	@RequestMapping("checkton")
	public m_storageonehand findcheck(@RequestParam long locatorid, @RequestParam long productid) {
		return ms.findByLocatoridAndProductid(locatorid, productid);
	}

	// POST
	@PostMapping
	@RequestMapping("create")
	public m_storageonehand createnew(@RequestParam long adorgid, @RequestParam long locatorid,
			@RequestParam long productid, @RequestParam long qty) {
		m_storageonehand m = new m_storageonehand();
		m.setId(1000003); // ad_client_id
		m.setCreated(g.getDate());
		m.setCreatedby(100);
		m.setAd_org_id(adorgid);
		m.setUpdated(g.getDate());
		m.setUpdatedby(100);
		m.setAsiid((long) 1354623);
		m.setLocatorid(locatorid);

		m.setM_storageonhand_uu(g.getUUID());
		m.setProductid(productid);
		m.setQtyonhand(qty);
		m.setDatematerialpolicy(Timestamp.valueOf(java.time.LocalDate.now() + " 00:00:00"));
		System.out.println("Giờ Convert: " + m.getDatematerialpolicy());
		return ms.saveAndFlush(m);
	}
	@GetMapping
	@RequestMapping("demo")
	public m_storageonehand demo(@RequestParam long locatorid,long productid) {
		return ms.findByLocatoridAndProductid(locatorid,productid);
	}
	@GetMapping
	@RequestMapping("alltk")
	public long getAllTK(@RequestParam long productid) {
		try {
			return ms.SumqtyTK(productid);
		} catch (Exception e) {
			return 0;
		}
		
	}
	@PutMapping
	@RequestMapping("updateqty")
	public m_storageonehand updateqty(@RequestParam long locatorid, @RequestParam long productid,
			@RequestParam long qty, @RequestParam String status) {
		m_storageonehand m = new m_storageonehand();
		m = ms.findByLocatoridAndProductid(locatorid, productid);
		m_storageonehand m1 = new m_storageonehand();
		m1 = m;
		long qtynew = 0;
		long oldqty = m1.getQtyonhand();
		System.out.println("AUAIKJMAOP:A: "+oldqty);
		if (status.equals("A")) {
			qtynew =(long) oldqty + qty;
		} else {
			qtynew =(long) oldqty - qty;
		}
		m.setQtyonhand(qtynew);
		m.setDatematerialpolicy(Timestamp.valueOf(java.time.LocalDate.now() + " 00:00:00"));
		m.setUpdated(g.getDate());
		m.setAsiid((long) 1354623);
		return ms.saveAndFlush(m);
	}
}
