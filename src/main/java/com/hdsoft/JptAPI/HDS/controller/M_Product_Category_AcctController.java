package com.hdsoft.JptAPI.HDS.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hdsoft.JptAPI.HDS.Repositories.Ad_orgRepository;
import com.hdsoft.JptAPI.HDS.Repositories.M_Product_Category_Acct_Rep;
import com.hdsoft.JptAPI.HDS.model.M_Product_Category_AcctModel;

@RestController
@RequestMapping("api/v1/m_product_category_acct")
public class M_Product_Category_AcctController {
	@Autowired
	M_Product_Category_Acct_Rep m1;
	@Autowired  
	Ad_orgRepository aor;
	GetIDUUDate g1 = new GetIDUUDate();
	@PostMapping
	@RequestMapping("create")
	public M_Product_Category_AcctModel creatednew(@RequestParam long m_product_category_id,@RequestParam long ad_org_id) {
		M_Product_Category_AcctModel m2 = new M_Product_Category_AcctModel();
		m2.setAd_client_id(1000003);
		m2.setM_product_category_acct_uu(g1.getUUID());
		m2.setAd_org_id(ad_org_id);
		m2.setC_acctschema_id(1000003);
		m2.setCreated(g1.getDate());
		m2.setCreatedby(100);
		m2.setUpdated(g1.getDate());
		m2.setUpdatedby(100);
		m2.setIsactive("Y");
		m2.setM_product_category_id(m_product_category_id);
		m2.setP_asset_acct(1001073);
		m2.setP_revenue_acct(1001087);
		m2.setP_expense_acct(1001079);
		m2.setP_cogs_acct(1001079);
		m2.setP_purchasepricevariance_acct(1001073);
		m2.setP_invoicepricevariance_acct(1001073);
		m2.setP_tradediscountrec_acct(1001073);
		m2.setP_tradediscountgrant_acct(1001088);
		m2.setP_inventoryclearingacct(1001080);
		m2.setP_costadjustment_acct(1001073);
		m2.setP_averagecostvariance_acct(1001073);
		m2.setP_landedcostclearing_acct(1001073);
		m2.setP_ratevariance_acct(1001086);
		return m1.saveAndFlush(m2);
	}
}
