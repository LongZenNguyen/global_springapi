package com.hdsoft.JptAPI.HDS.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hdsoft.JptAPI.HDS.model.M_InoutLineHoanThanh;
import com.hdsoft.JptAPI.HDS.model.m_inout;
import com.hdsoft.JptAPI.HDS.model.m_inoutline;
import com.hdsoft.JptAPI.HDS.model.m_transaction;
import com.hdsoft.JptAPI.Repositories.OrderlineRepository;
import com.hdsoft.JptAPI.HDS.Repositories.Ad_orgRepository;
import com.hdsoft.JptAPI.HDS.Repositories.C_UomRepository;
import com.hdsoft.JptAPI.HDS.Repositories.M_ProductPriceRepository;
import com.hdsoft.JptAPI.HDS.Repositories.m_inoutlineRepository;
import com.hdsoft.JptAPI.HDS.Repositories.m_locatorRepository;
import com.hdsoft.JptAPI.HDS.Repositories.m_productRepository;
import com.hdsoft.JptAPI.HDS.Repositories.m_transactionRepository;

@RestController
@RequestMapping("api/v1/lm_inoutline")
public class m_inoutlineController {
	GetIDUUDate g = new GetIDUUDate();
	@Autowired
	m_inoutlineRepository mLineRepository;
	@Autowired
	M_ProductPriceRepository mpp;
	@Autowired
	m_productRepository mp;
	@Autowired
	C_UomRepository cu;
	@Autowired
	m_locatorRepository mlr;
	@Autowired
	OrderlineRepository colr;
	@Autowired  
	Ad_orgRepository aor;
	//GET
	@GetMapping
	@RequestMapping(value = "/findisdisplay/{m_inout_id}")
	public List<m_inoutline> getDetailOfMInOut(@PathVariable long m_inout_id){
		List<m_inoutline> listFocusMInOutID = new ArrayList<m_inoutline>();
		for (m_inoutline m : mLineRepository.findAll() ) {
			if(m.getAd_client_id()== 1000014 && m.getM_inout_id()==m_inout_id) {
				listFocusMInOutID.add(m);
			}
		}
		return listFocusMInOutID;
		
	}
	@GetMapping
	public m_inoutline TopID(){
		return mLineRepository.findTopByOrderByIdDesc();
	}
	@GetMapping
	@RequestMapping(value = "/findmaxnumbergen/")
	public String findTopByOrderNumbergenByDesc1() {
		long numbermax=0;
		long number;
		String pattern="";
		for (m_inoutline m : mLineRepository.findByNumbergenIsNotNullAndAdclientid(1000014)) {
			System.out.println("Check : "+m.getAttributeinfor());
			number = Long.parseLong(m.getNumbergen().replace("'", ""));
			if(number>numbermax) numbermax=number;
		}
		System.out.println("Number Max: "+numbermax);
		return ""+numbermax;
	}
	@GetMapping
	@RequestMapping(value = "/findmaxnumbergen1/")
	public List<m_inoutline> findTopByOrderNumbergenByDesc2() {
		return mLineRepository.findByNumbergenIsNotNullAndAdclientid(1000014);
	}
	//POST
	@RequestMapping(method = RequestMethod.POST,value="/nhapkho")
	public m_inoutline insertNewDetailForMInOut(
			@RequestParam long m_inout_id,@RequestParam long m_product_id
			,@RequestParam long m_attributesetinstance_id,@RequestParam String attributeinfor,@RequestParam long qtyentered,
			@RequestParam long movementqty,@RequestParam long line,@RequestParam long c_orderline_id,@RequestParam String palletfrelix
			,@RequestParam String invoiceno,@RequestParam String numbergen
			,@RequestParam long locatorid,@RequestParam String carton,@RequestParam String orderno) {
		m_inoutline m = new m_inoutline();
		m.setId(g.getNextID("M_InOutLine"));	
		m.setAd_client_id((long) 1000014);
		m.setOrderno(orderno);
		m.setAd_org_id(1000039);
		m.setM_locator_id(locatorid);
		m.setCreated(g.getDate());
		m.setCreatedby((long) 1000080);
		m.setUpdated(g.getDate());
		m.setUpdatedby(1000080);
		m.setC_orderline_id(c_orderline_id);
		m.setM_inoutline_uu(g.getUUID());
		m.setLine(line);
		m.setDescription(null);
		m.setM_inout_id(m_inout_id);
		m.setM_product_id(m_product_id);
		m.setC_uom_id(100);
		m.setAttributeinfor(attributeinfor);
		m.setM_attributesetinstance_id(m_attributesetinstance_id);
		m.setQtyentered(qtyentered);
		m.setMovementqty(movementqty);
		m.setPalletfrelix(palletfrelix);
		m.setInvoiceno(invoiceno);
		m.setQtyctn(carton);
		m.setNumbergen(numbergen);
		return mLineRepository.saveAndFlush(m);
	}
	//Thạch Bàn
	@RequestMapping(method = RequestMethod.POST,value="/nhapkhoThachBan")
	public m_inoutline createDetailThachBan(
			@RequestParam long m_inout_id,@RequestParam long m_product_id
			,@RequestParam String attributeinfor,@RequestParam double qtyentered,
			@RequestParam double movementqty,@RequestParam long line,@RequestParam long c_orderline_id
			,@RequestParam long locatorid) {
		//locatorID=&productID=&quantity=&asiInfo=&uomID=m_inout_id=&c_orderline_id=&qtyover=&ad_client_id=ad_org_idad_user_id=
		m_inoutline m = new m_inoutline();
		m.setId(g.getNextID("M_InOutLine"));	
		m.setAd_client_id((long) 1000000);
		m.setAd_org_id(1000000);
		m.setM_locator_id(locatorid);
		m.setCreated(g.getDate());
		m.setCreatedby((long) 1000005);
		m.setUpdated(g.getDate());
		m.setUpdatedby(1000005);
		m.setC_orderline_id(c_orderline_id);
		m.setM_inoutline_uu(g.getUUID());
		m.setLine(line);
		m.setDescription(null);
		m.setM_inout_id(m_inout_id);
		m.setM_product_id(m_product_id);
		m.setC_uom_id(100);
		m.setAttributeinfor(attributeinfor);
//		m.setM_attributesetinstance_id(m_attributesetinstance_id);
		m.setQtyentered(qtyentered);
		m.setMovementqty(movementqty);
		return mLineRepository.saveAndFlush(m);
	}
	
	//HDS Base App
	//GET
	@GetMapping
	@RequestMapping("detail")
	public List<M_InoutLineHoanThanh> detail(
			@RequestParam long m_inout_id,@RequestParam String statusMInOut,@RequestParam long adorgid) {
	List<M_InoutLineHoanThanh> list = new ArrayList<M_InoutLineHoanThanh>();
	String orgDes = aor.getDesOrg(adorgid);
	for (m_inoutline mi : mLineRepository.findByMinoutid(m_inout_id)) {
		M_InoutLineHoanThanh mi1 = new M_InoutLineHoanThanh();
		mi1.setM_inout_id(mi.getM_inout_id());
		mi1.setProductID(mi.getM_product_id());
		mi1.setProductName(mp.getOne(mi.getM_product_id()).getValue().replaceAll("-"+orgDes, ""));
		mi1.setQty((long) mi.getQtyentered());
		mi1.setUomid(mi.getC_uom_id());
		mi1.setUomName(cu.getOne(mi.getC_uom_id()).getName().replaceAll("-"+orgDes, ""));
		mi1.setUpdated((Timestamp) mi.getUpdated());
		mi1.setM_locator_id(mi.getM_locator_id());
		mi1.setLocatorName(mlr.getOne(mi1.getM_locator_id()).getValue().replaceAll("-"+orgDes, ""));
		if (statusMInOut.equals("nh")) {
			try {
				mi1.setTotalprice((long) (mpp.findByProductid(mi.getM_product_id()).get(0).getPricestd()*mi.getQtyentered()));
				
			} catch (Exception e) {
				mi1.setTotalprice(null);
			}
		}else if(statusMInOut.equals("xh")) {
			try {
				mi1.setTotalprice((long) (mpp.findByProductid(mi.getM_product_id()).get(0).getPricelist()*mi.getQtyentered()));
				
			} catch (Exception e) {
				mi1.setTotalprice(null);
			}
		}
		list.add(mi1);
	}
	return list;
	}
	
	
	///////////////////////
	@RequestMapping(method = RequestMethod.POST,value="/detail")
	public m_inoutline create(
			@RequestParam long m_inout_id,@RequestParam long m_product_id
			,@RequestParam long qtyentered,
			@RequestParam long movementqty,@RequestParam long line
			,@RequestParam long locatorid,@RequestParam long adorgid,@RequestParam long priceedit) {
		m_inoutline m = new m_inoutline();
		m.setId(g.getNextID("M_InOutLine"));	
		m.setAd_client_id((long) 1000003);
		m.setOrderno(null);
		m.setAd_org_id(adorgid);
		m.setM_locator_id(locatorid);
		m.setCreated(g.getDate());
		m.setCreatedby((long) 100);
		m.setUpdated(g.getDate());
		m.setUpdatedby(100);
		m.setC_orderline_id(null);
		m.setM_inoutline_uu(g.getUUID());
		m.setLine(line);
		m.setDescription(null);
		m.setM_inout_id(m_inout_id);
		m.setM_product_id(m_product_id);
		m.setC_uom_id(100);
		m.setAttributeinfor(null);
		m.setM_attributesetinstance_id(null);
		m.setQtyentered(qtyentered);
		m.setMovementqty(movementqty);
		m.setPalletfrelix(null);
		m.setInvoiceno(null);
		m.setQtyctn(null);
		m.setNumbergen(""+priceedit);
		return mLineRepository.saveAndFlush(m);
	}
	@RequestMapping(method = RequestMethod.POST,value="/detailxk")
	public m_inoutline createdetailxk(
			@RequestParam long m_inout_id,@RequestParam long m_product_id
			,@RequestParam long qtyentered,
			@RequestParam long movementqty,@RequestParam long line
			,@RequestParam long locatorid,@RequestParam long adorgid,@RequestParam long c_order_id) {
		m_inoutline m = new m_inoutline();
		m.setId(g.getNextID("M_InOutLine"));	
		m.setAd_client_id((long) 1000003);
		m.setOrderno(null);
		m.setAd_org_id(adorgid);
		m.setM_locator_id(locatorid);
		m.setCreated(g.getDate());
		m.setCreatedby((long) 100);
		m.setUpdated(g.getDate());
		m.setUpdatedby(100);
		try {
			m.setC_orderline_id(colr.findByOrderIDAndSanPham(c_order_id, m_product_id).get(0).getId());
		} catch (Exception e) {
			m.setC_orderline_id(null);
			// TODO: handle exception
		}
		m.setM_inoutline_uu(g.getUUID());
		m.setLine(line);
		m.setDescription(null);
		m.setM_inout_id(m_inout_id);
		m.setM_product_id(m_product_id);
		m.setC_uom_id(100);
		m.setAttributeinfor(null);
		m.setM_attributesetinstance_id(null);
		m.setQtyentered(qtyentered);
		m.setMovementqty(movementqty);
		m.setPalletfrelix(null);
		m.setInvoiceno(null);
		m.setQtyctn(null);
		m.setNumbergen(null);
		return mLineRepository.saveAndFlush(m);
	}
	
	
}
