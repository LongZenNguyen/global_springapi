package com.hdsoft.JptAPI.HDS.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hdsoft.JptAPI.HDS.Repositories.Ad_orgRepository;
import com.hdsoft.JptAPI.HDS.Repositories.C_UomRepository;
import com.hdsoft.JptAPI.HDS.Repositories.M_MovementLineRepository;
import com.hdsoft.JptAPI.HDS.Repositories.M_MovementRepository;
import com.hdsoft.JptAPI.HDS.Repositories.m_locatorRepository;
import com.hdsoft.JptAPI.HDS.Repositories.m_productRepository;
import com.hdsoft.JptAPI.HDS.model.M_MovementlineInfomation;
import com.hdsoft.JptAPI.HDS.model.m_movementLine;

@RestController
@RequestMapping("api/v1/movementlineinfo")
public class M_MovementLineInfomationController {
	@Autowired
	m_productRepository mp;
	@Autowired
	m_locatorRepository ml;
	@Autowired
	C_UomRepository cu;
	@Autowired
	M_MovementLineRepository mm;
	@Autowired  
	Ad_orgRepository aor;
	@Autowired
	M_MovementRepository mmr;
	
	@GetMapping
	@RequestMapping("allinfo")
	public List<M_MovementlineInfomation> findAllInfo(@RequestParam long movementid){
		List<M_MovementlineInfomation> list = new ArrayList<M_MovementlineInfomation>();
		String orgDes = aor.getDesOrg( mmr.getOne(movementid).getAd_org_id());
		for (m_movementLine	 mml : mm.findByMovementId(movementid)) {
			M_MovementlineInfomation mInfomation = new M_MovementlineInfomation();
			mInfomation.setC_uom_id( mp.findById(mml.getProductId()).get().getC_uom_id());
			mInfomation.setLocatorname(ml.findById(mml.getCurrentLocatorId()).get().getValue().replaceAll("-"+orgDes, ""));
			mInfomation.setLocatortoname(ml.findById(mml.getLocatorToId()).get().getValue().replaceAll("-"+orgDes, ""));
			mInfomation.setM_locator_id(mml.getCurrentLocatorId());
			mInfomation.setM_locatorto_id(mml.getLocatorToId());
			mInfomation.setM_product_id(mml.getProductId());
			mInfomation.setMovementId(mml.getMovementId());
			mInfomation.setMovementLineId(mml.getMovementLineId());
			mInfomation.setPrice(0);// không tính 
			mInfomation.setProductname(mp.findById(mml.getProductId()).get().getValue().replaceAll("-"+orgDes, ""));
			mInfomation.setQty((long) mml.getQuantity());
			mInfomation.setUomName(cu.findById(mp.findById(mml.getProductId()).get().getC_uom_id()).get().getName().replaceAll("-"+orgDes, ""));
			list.add(mInfomation);
		}
		return list;
	}
}
