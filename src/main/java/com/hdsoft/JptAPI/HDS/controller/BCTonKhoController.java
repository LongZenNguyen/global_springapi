package com.hdsoft.JptAPI.HDS.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hdsoft.JptAPI.HDS.Repositories.Ad_orgRepository;
import com.hdsoft.JptAPI.HDS.Repositories.m_attributesetinstanceRepository;
import com.hdsoft.JptAPI.HDS.Repositories.m_locatorRepository;
import com.hdsoft.JptAPI.HDS.Repositories.m_storageonhandRepository;
import com.hdsoft.JptAPI.HDS.model.TonKhoModel;
import com.hdsoft.JptAPI.HDS.model.m_locator;
import com.hdsoft.JptAPI.HDS.model.m_storageonehand;
import com.hdsoft.JptAPI.Models.Locator;
import com.hdsoft.JptAPI.Repositories.ProductRepository;

@RestController
@RequestMapping("api/v1/baocaotonkho")
public class BCTonKhoController {
	@Autowired
	ProductRepository productRep;
	@Autowired
	m_locatorRepository locatorRep;
	@Autowired
	m_storageonhandRepository storageRep;
	@Autowired
	m_attributesetinstanceRepository asiRep;

	@Autowired
	Ad_orgRepository aor;

//	===================GET===================
	@GetMapping
	public List<TonKhoModel> showBCTonKho() {
		List<TonKhoModel> ListShow = new ArrayList<TonKhoModel>();
		String orgDes="";
		for (m_storageonehand m : storageRep.findByAdclientidAndQtyonhandGreaterThan(1000000, 0)) {
			if (("" + m.getAsiid()).length() != 0) {
				TonKhoModel tk = new TonKhoModel();
				orgDes=aor.getDesOrg(m.getAd_org_id());
				tk.setViTri(locatorRep.findById(m.getLocatorid()).getValue().replaceAll("-" + orgDes, ""));
				tk.setSanPham(productRep.findById(m.getProductid()).getValue().replaceAll("-" + orgDes, ""));
				tk.setSoLuong(m.getQtyonhand());
				tk.setNgaySX(asiRep.findById(m.getAsiid()).get().getDescription());
				ListShow.add(tk);
			}
		}
		return ListShow;
	}

	@GetMapping
	@RequestMapping("/baocaovitritrong")
	public List<m_locator> showBCViTriTrong() {
//		List<TonKhoModel> showList = new ArrayList<TonKhoModel>();
//		List<m_storageonehand> listStorageonHand = new ArrayList<m_storageonehand>();
		List<m_locator> listLocator = new ArrayList<m_locator>();
		listLocator = locatorRep.findAllByAdclientid(1000000);
		List<m_locator> showL = new ArrayList<m_locator>();
		showL = locatorRep.findAllByAdclientid(1000000);
		String orgDes = "";
		if (showL.size() > 0)
			orgDes = aor.getDesOrg(showL.get(0).getAd_org_id());
		for (m_storageonehand m : storageRep.findByAdclientidAndQtyonhandGreaterThan(1000000, 0)) {
			for (m_locator ml : listLocator) {
				if (m.getLocatorid() == ml.getId()) {
					showL.remove(ml);
				}
			}

		}
		for (m_locator m_locator : showL) {
			m_locator.setName(m_locator.getName().replaceAll("-" + orgDes, ""));
			m_locator.setValue(m_locator.getValue().replaceAll("-" + orgDes, ""));
		}
		return showL;
	}

	// Thạch Bàn
	@GetMapping
	@RequestMapping("/findtest")
	public List<m_storageonehand> fi() {
		return storageRep.findByAdclientidAndQtyonhandGreaterThan(1000000, 0);
	}

	@GetMapping
	@RequestMapping("/findbylocator")
	public List<TonKhoModel> FindByLocator(@RequestParam long locatorid) {
		List<TonKhoModel> listshowKhoModels = new ArrayList<TonKhoModel>();
		List<Locator> locatorList = new ArrayList<Locator>();
		String orgDes;
		for (m_storageonehand m : storageRep.findByAdclientidAndLocatoridAndQtyonhandGreaterThan(1000000, locatorid,
				0)) {
			TonKhoModel tKhoModel = new TonKhoModel();
			orgDes = aor.getDesOrg(m.getAd_org_id());
			tKhoModel.setNgaySX(asiRep.findById(m.getAsiid()).get().getLot());
			tKhoModel.setASIID(m.getAsiid());
			tKhoModel.setLocatorid(m.getLocatorid());
			tKhoModel.setProductID(m.getProductid());
			tKhoModel.setSanPham(productRep.findById(m.getProductid()).getValue().replaceAll("-" + orgDes, ""));
			tKhoModel.setSoLuong(m.getQtyonhand());
			tKhoModel.setViTri(locatorRep.findById(m.getLocatorid()).getValue().replaceAll("-" + orgDes, ""));
			listshowKhoModels.add(tKhoModel);
		}
		return listshowKhoModels;
	}

	// Thach ban
	@GetMapping
	@RequestMapping("/findbylocatorid")
	public List<TonKhoModel> FindByLocatorid(@RequestParam long locatorid) {
		List<TonKhoModel> listshowKhoModels = new ArrayList<TonKhoModel>();
		List<Locator> locatorList = new ArrayList<Locator>();
		String orgDes = "";
		for (m_storageonehand m : storageRep.findByAdclientidAndLocatoridAndQtyonhandGreaterThan(1000000, locatorid,
				0)) {
			TonKhoModel tKhoModel = new TonKhoModel();
			orgDes = aor.getDesOrg(m.getAd_org_id());
			tKhoModel.setNgaySX(asiRep.findById(m.getAsiid()).get().getDescription());
			tKhoModel.setASIID(m.getAsiid());
			tKhoModel.setLocatorid(m.getLocatorid());
			tKhoModel.setProductID(m.getProductid());
			tKhoModel.setSanPham(productRep.findById(m.getProductid()).getValue().replaceAll("-" + orgDes, ""));
			tKhoModel.setSoLuong(m.getQtyonhand());
			tKhoModel.setViTri(locatorRep.findById(m.getLocatorid()).getValue().replaceAll("-" + orgDes, ""));
			listshowKhoModels.add(tKhoModel);
		}
		return listshowKhoModels;
	}

	// HDS Base APP
	@GetMapping
	@RequestMapping("/timkiemtheosanphamvachinhanh")
	public List<TonKhoModel> timkiemtheoSPAndChiNhanh(@RequestParam long ad_org_id) {
		List<TonKhoModel> listShow = new ArrayList<TonKhoModel>();
		String orgDes = aor.getDesOrg(ad_org_id);
		for (m_storageonehand st : storageRep.findByAdorgidAndAdclientidAndQtyonhandGreaterThan(ad_org_id, 1000003,
				0)) {
			TonKhoModel tk = new TonKhoModel();
			tk.setASIID(st.getAsiid());
			tk.setLocatorid(st.getLocatorid());
			tk.setNgaySX(asiRep.findById(st.getAsiid()).get().getLot());
			tk.setProductID(st.getProductid());
			tk.setSanPham(productRep.findById(st.getProductid()).getValue().replaceAll("-" + orgDes, ""));
			tk.setSoLuong(st.getQtyonhand());
			tk.setViTri(locatorRep.findById(st.getLocatorid()).getValue().replaceAll("-" + orgDes, ""));
			listShow.add(tk);
		}
		return listShow;
	}

	@GetMapping
	@RequestMapping("/tonkhosanpham")
	public List<TonKhoModel> timkiemtheoSPTK(@RequestParam long productid) {
		List<TonKhoModel> listShow = new ArrayList<TonKhoModel>();

		for (m_storageonehand st : storageRep.findByProductidAndQtyonhandGreaterThan(productid, 0)) {
			TonKhoModel tk = new TonKhoModel();
			tk.setASIID(st.getAsiid());
			tk.setLocatorid(st.getLocatorid());
			tk.setNgaySX(asiRep.findById(st.getAsiid()).get().getLot());
			tk.setProductID(st.getProductid());
			String orgDes = aor.getDesOrg(st.getAd_org_id());
			tk.setSanPham(productRep.findById(st.getProductid()).getValue().replaceAll("-" + orgDes, ""));
			tk.setSoLuong(st.getQtyonhand());
			tk.setViTri(locatorRep.findById(st.getLocatorid()).getValue().replaceAll("-" + orgDes, ""));
			listShow.add(tk);
		}
		return listShow;
	}

	@GetMapping
	@RequestMapping("/findbylocator2")
	public List<m_storageonehand> abc(@RequestParam long locatorid) {
		return storageRep.findByLocatorid(locatorid);
	}

	@GetMapping
	@RequestMapping("/findbylocator1")
	public List<TonKhoModel> FindByLocato1r(@RequestParam long locatorid, @RequestParam long adorgid) {
		List<TonKhoModel> listshowKhoModels = new ArrayList<TonKhoModel>();
		List<Locator> locatorList = new ArrayList<Locator>();
		String orgDes = aor.getDesOrg(adorgid);
		for (m_storageonehand m : storageRep.findByLocatoridAndAdorgidAndQtyonhandGreaterThan(locatorid, adorgid, 0)) {
			TonKhoModel tKhoModel = new TonKhoModel();
			tKhoModel.setNgaySX(asiRep.findById(m.getAsiid()).get().getLot());
			tKhoModel.setASIID(m.getAsiid());
			tKhoModel.setLocatorid(m.getLocatorid());
			tKhoModel.setProductID(m.getProductid());
			tKhoModel.setSanPham(productRep.findById(m.getProductid()).getValue().replaceAll("-" + orgDes, ""));
			tKhoModel.setSoLuong(m.getQtyonhand());
			tKhoModel.setViTri(locatorRep.findById(m.getLocatorid()).getValue().replaceAll("-" + orgDes, ""));
			listshowKhoModels.add(tKhoModel);
		}
		return listshowKhoModels;
	}

	//
}
