package com.hdsoft.JptAPI.HDS.controller;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hdsoft.JptAPI.HDS.Repositories.M_InventoryRepository;
import com.hdsoft.JptAPI.HDS.model.M_Inventory;

@RestController
@RequestMapping("api/v1/m_inventory")
public class M_InventoryController {
	GetIDUUDate g = new GetIDUUDate();
	@Autowired
	M_InventoryRepository mirInventoryRepository;
	
	@GetMapping
	public List<M_Inventory> findTopByDocumentno() {
		return mirInventoryRepository.findAllByAdclientid(1000014);
	}
	//Kiem kho Thach Ban
	@PostMapping
	@RequestMapping("/createkiemkho")
	public M_Inventory createInventory(@RequestParam String documentno) {
		M_Inventory m_Inventory = new M_Inventory();
		m_Inventory.setAd_client_id((long) 1000000);
		m_Inventory.setAd_org_id((long) 1000000);
		m_Inventory.setCreated(g.getDate());
		m_Inventory.setUpdated(g.getDate());
		m_Inventory.setUpdatedby(1000005);
		m_Inventory.setCreatedby(1000005);
		m_Inventory.setMovementdate(g.getDate());
		m_Inventory.setDocstatus("DR");
		m_Inventory.setDocaction("CO");
		m_Inventory.setC_doctype_id(1000023);
		m_Inventory.setId(g.getNextID("M_Inventory"));
		m_Inventory.setM_Inventory_uu(g.getUUID());
		m_Inventory.setDocumentno(documentno);
		m_Inventory.setM_warehouse_id(1000000);
		m_Inventory.setProcessing("N");
		m_Inventory.setGeneratelist("N");
		m_Inventory.setApprovalamt(0);
		return mirInventoryRepository.saveAndFlush(m_Inventory);
	}
	//Get max documentno của m_inventory dùng cho mọi client , mọi dạng documentno
	@GetMapping
	@RequestMapping("getmaxdocumentno")
	public M_Inventory getMaxDocumentNo(@RequestParam String documentno) {
		if(mirInventoryRepository.findByDocumentnoStartsWith(documentno).size()==0) {
			return null;
		}else {
			return mirInventoryRepository.findByDocumentnoStartsWith(documentno).get(mirInventoryRepository.findByDocumentnoStartsWith(documentno).size()-1);
		}
	}
	
}
