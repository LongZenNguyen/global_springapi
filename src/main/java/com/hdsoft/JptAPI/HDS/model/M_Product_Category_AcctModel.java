package com.hdsoft.JptAPI.HDS.model;


import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@Entity(name = "M_Product_Category_Acct")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class M_Product_Category_AcctModel {
	@Id
	private String m_product_category_acct_uu;
	private long m_product_category_id;
	private long c_acctschema_id;
	@Column(name="ad_client_id")
	private long ad_client_id;
	private long ad_org_id;
	private String isactive;
	private long p_tradediscountgrant_acct;
	private Timestamp created;
	private long createdby;
	private Timestamp updated;
	private long updatedby;
	private long p_revenue_acct;
	private long p_expense_acct;
	private long p_asset_acct;
	private long p_cogs_acct;
	private long p_purchasepricevariance_acct;
	private int p_invoicepricevariance_acct;
	private long p_tradediscountrec_acct;
	private long p_inventoryclearing_acct;
	private long p_costadjustment_acct;
	private long p_ratevariance_acct;
	private long p_averagecostvariance_acct;
	
	private long p_landedcostclearing_acct;
	public long getM_product_category_id() {
		return m_product_category_id;
	}
	public void setM_product_category_id(long m_product_category_id) {
		this.m_product_category_id = m_product_category_id;
	}
	public long getC_acctschema_id() {
		return c_acctschema_id;
	}
	public void setC_acctschema_id(long c_acctschema_id) {
		this.c_acctschema_id = c_acctschema_id;
	}
	public long getAd_client_id() {
		return ad_client_id;
	}
	public void setAd_client_id(long ad_client_id) {
		this.ad_client_id = ad_client_id;
	}
	public long getAd_org_id() {
		return ad_org_id;
	}
	public void setAd_org_id(long ad_org_id) {
		this.ad_org_id = ad_org_id;
	}
	public String getIsactive() {
		return isactive;
	}
	public void setIsactive(String isactive) {
		this.isactive = isactive;
	}
	public Timestamp getCreated() {
		return created;
	}
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	public long getCreatedby() {
		return createdby;
	}
	public void setCreatedby(long createdby) {
		this.createdby = createdby;
	}
	public Timestamp getUpdated() {
		return updated;
	}
	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}
	public long getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(long updatedby) {
		this.updatedby = updatedby;
	}
	public long getP_revenue_acct() {
		return p_revenue_acct;
	}
	public void setP_revenue_acct(long p_revenue_acct) {
		this.p_revenue_acct = p_revenue_acct;
	}
	public long getP_expense_acct() {
		return p_expense_acct;
	}
	public void setP_expense_acct(long p_expense_acct) {
		this.p_expense_acct = p_expense_acct;
	}
	public long getP_asset_acct() {
		return p_asset_acct;
	}
	public void setP_asset_acct(long p_asset_acct) {
		this.p_asset_acct = p_asset_acct;
	}
	public long getP_cogs_acct() {
		return p_cogs_acct;
	}
	public void setP_cogs_acct(long p_cogs_acct) {
		this.p_cogs_acct = p_cogs_acct;
	}
	public int getP_purchasepricevariance_acct() {
		return (int) p_purchasepricevariance_acct;
	}
	public void setP_purchasepricevariance_acct(long p_purchasepricevarience_acct) {
		this.p_purchasepricevariance_acct = p_purchasepricevarience_acct;
	}
	public long getP_invoicepricevariance_acct() {
		return p_invoicepricevariance_acct;
	}
	public void setP_invoicepricevariance_acct(int p_invoicepricevariance_acct) {
		this.p_invoicepricevariance_acct = p_invoicepricevariance_acct;
	}
	public long getP_tradediscountrec_acct() {
		return p_tradediscountrec_acct;
	}
	public void setP_tradediscountrec_acct(long p_tradediscountrec_acct) {
		this.p_tradediscountrec_acct = p_tradediscountrec_acct;
	}
	public long getP_inventoryclearingacct() {
		return p_inventoryclearing_acct;
	}
	public void setP_inventoryclearingacct(long p_inventoryclearingacct) {
		this.p_inventoryclearing_acct = p_inventoryclearingacct;
	}
	public long getP_costadjustment_acct() {
		return p_costadjustment_acct;
	}
	public void setP_costadjustment_acct(long p_costadjustment_acct) {
		this.p_costadjustment_acct = p_costadjustment_acct;
	}
	public long getP_ratevariance_acct() {
		return p_ratevariance_acct;
	}
	public void setP_ratevariance_acct(long p_ratevariance_acct) {
		this.p_ratevariance_acct = p_ratevariance_acct;
	}
	public long getP_averagecostvariance_acct() {
		return p_averagecostvariance_acct;
	}
	public void setP_averagecostvariance_acct(long p_averagecostvariance_acct) {
		this.p_averagecostvariance_acct = p_averagecostvariance_acct;
	}
	public String getM_product_category_acct_uu() {
		return m_product_category_acct_uu;
	}
	public void setM_product_category_acct_uu(String p_product_category_acct_uu) {
		this.m_product_category_acct_uu = p_product_category_acct_uu;
	}
	public long getP_landedcostclearing_acct() {
		return p_landedcostclearing_acct;
	}
	public void setP_landedcostclearing_acct(long p_landedcostclearing_acct) {
		this.p_landedcostclearing_acct = p_landedcostclearing_acct;
	}
	public long getP_tradediscountgrant_acct() {
		return p_tradediscountgrant_acct;
	}
	public void setP_tradediscountgrant_acct(long p_tradediscountgrant_acct) {
		this.p_tradediscountgrant_acct = p_tradediscountgrant_acct;
	}
	
	
}
