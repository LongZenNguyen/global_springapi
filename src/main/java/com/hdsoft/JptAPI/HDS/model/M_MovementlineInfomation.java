 package com.hdsoft.JptAPI.HDS.model;

public class M_MovementlineInfomation {
	private long movementLineId;
	private long m_product_id;
	private String productname;
	private long c_uom_id;
	private String uomName;
	private long qty;
	private long m_locator_id;
	private String locatorname;
	private long m_locatorto_id;
	private String locatortoname;
	private long movementId;
	private long price;
	public long getMovementLineId() {
		return movementLineId;
	}
	public void setMovementLineId(long movementLineId) {
		this.movementLineId = movementLineId;
	}
	public long getM_product_id() {
		return m_product_id;
	}
	public void setM_product_id(long m_product_id) {
		this.m_product_id = m_product_id;
	}
	public String getProductname() {
		return productname;
	}
	public void setProductname(String productname) {
		this.productname = productname;
	}
	public long getC_uom_id() {
		return c_uom_id;
	}
	public void setC_uom_id(long c_uom_id) {
		this.c_uom_id = c_uom_id;
	}
	public String getUomName() {
		return uomName;
	}
	public void setUomName(String uomName) {
		this.uomName = uomName;
	}
	public long getQty() {
		return qty;
	}
	public void setQty(long qty) {
		this.qty = qty;
	}
	public long getM_locator_id() {
		return m_locator_id;
	}
	public void setM_locator_id(long m_locator_id) {
		this.m_locator_id = m_locator_id;
	}
	public String getLocatorname() {
		return locatorname;
	}
	public void setLocatorname(String locatorname) {
		this.locatorname = locatorname;
	}
	public long getM_locatorto_id() {
		return m_locatorto_id;
	}
	public void setM_locatorto_id(long m_locatorto_id) {
		this.m_locatorto_id = m_locatorto_id;
	}
	public String getLocatortoname() {
		return locatortoname;
	}
	public void setLocatortoname(String locatortoname) {
		this.locatortoname = locatortoname;
	}
	public long getMovementId() {
		return movementId;
	}
	public void setMovementId(long movementId) {
		this.movementId = movementId;
	}
	public long getPrice() {
		return price;
	}
	public void setPrice(long price) {
		this.price = price;
	}
}
