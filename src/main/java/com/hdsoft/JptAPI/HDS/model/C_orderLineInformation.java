package com.hdsoft.JptAPI.HDS.model;

public class C_orderLineInformation {
	private long c_orderline_id;
	private long m_product_id;
	private String productvalue;
	private double qtyentered;
	private double qtyreceived;
	private Double qtythieu;
	private String status;
	
	public Double getQtythieu() {
		return qtythieu;
	}
	public void setQtythieu(Double qtythieu) {
		this.qtythieu = qtythieu;
	}
	public long getC_orderline_id() {
		return c_orderline_id;
	}
	public void setC_orderline_id(long c_orderline_id) {
		this.c_orderline_id = c_orderline_id;
	}
	public long getM_product_id() {
		return m_product_id;
	}
	public void setM_product_id(long m_product_id) {
		this.m_product_id = m_product_id;
	}
	public String getProductvalue() {
		return productvalue;
	}
	public void setProductvalue(String productvalue) {
		this.productvalue = productvalue;
	}
	public double getQtyentered() {
		return qtyentered;
	}
	public void setQtyentered(double qtyentered) {
		this.qtyentered = qtyentered;
	}
	public double getQtyreceived() {
		return qtyreceived;
	}
	public void setQtyreceived(double qtyreceived) {
		this.qtyreceived = qtyreceived;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
