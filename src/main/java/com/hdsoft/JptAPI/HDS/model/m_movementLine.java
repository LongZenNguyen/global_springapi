 package com.hdsoft.JptAPI.HDS.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "m_movementline")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class m_movementLine {

	@Id 
	@Column(name = "m_movementline_id")
	private Long movementLineId;

	@Column(name = "m_locator_id")
	private Long currentLocatorId;

	@Column(name = "m_locatorto_id")
	private Long locatorToId;

	@Column(name = "m_product_id")
	private Long productId;

	@Column(name = "movementqty")
	private double quantity;

	@Column(name = "m_attributesetinstance_id")
	private Long attributeSetId;

	@Column(name = "m_movement_id")
	private Long movementId;

	private String m_movementline_uu;
	public String getM_movementline_uu() {
		return m_movementline_uu;
	}

	public void setM_movementline_uu(String m_movementline_uu) {
		this.m_movementline_uu = m_movementline_uu;
	}

	public m_movementLine() {
		super();
	}

	public m_movementLine(Long movementLineId, Long currentLocatorId, Long locatorToId, Long productId, double quantity,
			Long attributeSetId, Long movementId) {
		super();
		this.movementLineId = movementLineId;
		this.currentLocatorId = currentLocatorId;
		this.locatorToId = locatorToId;
		this.productId = productId;
		this.quantity = quantity;
		this.attributeSetId = attributeSetId;
		this.movementId = movementId;
	}

	public Long getMovementLineId() {
		return movementLineId;
	}

	public void setMovementLineId(Long movementLineId) {
		this.movementLineId = movementLineId;
	}

	public Long getCurrentLocatorId() {
		return currentLocatorId;
	}

	public void setCurrentLocatorId(Long currentLocatorId) {
		this.currentLocatorId = currentLocatorId;
	}

	public Long getLocatorToId() {
		return locatorToId;
	}

	public void setLocatorToId(Long locatorToId) {
		this.locatorToId = locatorToId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public Long getAttributeSetId() {
		return attributeSetId;
	}

	public void setAttributeSetId(Long attributeSetId) {
		this.attributeSetId = attributeSetId;
	}

	public Long getMovementId() {
		return movementId;
	}

	public void setMovementId(Long movementId) {
		this.movementId = movementId;
	}

}
