 package com.hdsoft.JptAPI.HDS.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@Entity(name = "hds_pushnotification")
@Table(name = "hds_pushnotification")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class hds_pushnotification {
	
	@Id
	private long id;
	private String client_id;
	private String serialnumber;
	private String noidung;
	
	private Boolean isdisplay;
	
	public long getId() {
		return id;
	}
	public String getNoidung() {
		return noidung;
	}
	public void setNoidung(String noidung) {
		this.noidung = noidung;
	}
	public Boolean isIsdisplay() {
		return isdisplay;
	}
	public void setIsdisplay(Boolean isdisplay) {
		this.isdisplay = isdisplay;
	}
	public void setId(long id) {
		this.id = id;
	}
	public hds_pushnotification() {
		// TODO Auto-generated constructor stub
	}
	public String getClient_id() {
		return client_id;
	}
	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}
	public String getSerialnumber() {
		return serialnumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialnumber = serialNumber;
	}
	public hds_pushnotification(String client_id, String serialNumber,long id,String noidung,Boolean isdisplay) {
		super();
		this.client_id = client_id;
		this.serialnumber = serialNumber;
		this.id = id;
		this.isdisplay = isdisplay;
		this.noidung = noidung;
	}
	
}
