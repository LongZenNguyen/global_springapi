 package com.hdsoft.JptAPI.HDS.model;

import java.util.Date;

public class TonKhoModel {
	private String ViTri;
	private Long locatorid;
	private String SanPham;
	private Long productID;
	private String NgaySX;
	private Long ASIID;
	private long SoLuong;
	private String documentno;
	private String doitac;
	private long pricebuy;
	private long purchaseprice;
	
	public long getPricebuy() {
		return pricebuy;
	}
	public void setPricebuy(long pricebuy) {
		this.pricebuy = pricebuy;
	}
	public long getPurchaseprice() {
		return purchaseprice;
	}
	public void setPurchaseprice(long purchaseprice) {
		this.purchaseprice = purchaseprice;
	}
	public String getDoitac() {
		return doitac;
	}
	public void setDoitac(String doitac) {
		this.doitac = doitac;
	}
	public String getDocumentno() {
		return documentno;
	}
	public void setDocumentno(String documentno) {
		this.documentno = documentno;
	}
	public Long getLocatorid() {
		return locatorid;
	}
	public void setLocatorid(Long locatorid) {
		this.locatorid = locatorid;
	}
	public Long getProductID() {
		return productID;
	}
	public void setProductID(Long productID) {
		this.productID = productID;
	}
	public Long getASIID() {
		return ASIID;
	}
	public void setASIID(Long aSIID) {
		ASIID = aSIID;
	}
	public String getViTri() {
		return ViTri;
	}
	public void setViTri(String viTri) {
		ViTri = viTri;
	}
	public String getSanPham() {
		return SanPham;
	}
	public void setSanPham(String sanPham) {
		SanPham = sanPham;
	}
	public String getNgaySX() {
		return NgaySX;
	}
	public void setNgaySX(String ngaySX) {
		NgaySX = ngaySX;
	}
	public long getSoLuong() {
		return SoLuong;
	}
	public void setSoLuong(long soLuong) {
		SoLuong = soLuong;
	}
	
}
