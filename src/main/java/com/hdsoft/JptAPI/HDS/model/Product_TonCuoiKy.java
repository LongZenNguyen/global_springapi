package com.hdsoft.JptAPI.HDS.model;

public class Product_TonCuoiKy {
	private long priductid;
	private String productName;
	private double qty;
	private String locatorName;
	private long locatorid;
	public long getPriductid() {
		return priductid;
	}
	public void setPriductid(long priductid) {
		this.priductid = priductid;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public double getQty() {
		return qty;
	}
	public void setQty(double qty) {
		this.qty = qty;
	}
	public String getLocatorName() {
		return locatorName;
	}
	public void setLocatorName(String locatorName) {
		this.locatorName = locatorName;
	}
	public long getLocatorid() {
		return locatorid;
	}
	public void setLocatorid(long locatorid) {
		this.locatorid = locatorid;
	}
	
}
