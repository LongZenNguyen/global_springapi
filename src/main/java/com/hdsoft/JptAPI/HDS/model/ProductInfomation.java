package com.hdsoft.JptAPI.HDS.model;

import java.sql.Timestamp;

public class ProductInfomation {
	private long productID; 
	private String productName;
	private String productValue;
	private long unisperpack;
	private long clientid;
	private long orgid;
	private Long m_attributeset_id;//id thuoc tinh
	private String thuoctinh;
	private long uomID;
	private String dvt;
	private long productCategoryid;
	private String productCategoryName;
	private long pricesale;
	private long purprice;
	private long qty;
	private long totalQtysale;
	
	private long totalqtypur;
	private long locatorid;
	private String locatorName;
	private Timestamp created;
	private long c_orderline_id;
	private String note;
	
	
	
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public long getC_orderline_id() {
		return c_orderline_id;
	}
	public void setC_orderline_id(long c_orderline_id) {
		this.c_orderline_id = c_orderline_id;
	}
	public Timestamp getCreated() {
		return created;
	}
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	public long getLocatorid() {
		return locatorid;
	}
	public void setLocatorid(long locatorid) {
		this.locatorid = locatorid;
	}
	public String getLocatorName() {
		return locatorName;
	}
	public void setLocatorName(String locatorName) {
		this.locatorName = locatorName;
	}
	public long getTotalQtysale() {
		return totalQtysale;
	}
	public void setTotalQtysale(long totalQtysale) {
		this.totalQtysale = totalQtysale;
	}
	public long getTotalqtypur() {
		return totalqtypur;
	}
	public void setTotalqtypur(long totalqtypur) {
		this.totalqtypur = totalqtypur;
	}
	public long getQty() {
		return qty;
	}
	public void setQty(long qty) {
		this.qty = qty;
	}
	public long getPricesale() {
		return pricesale;
	}
	public void setPricesale(long pricesale) {
		this.pricesale = pricesale;
	}
	public long getPurprice() {
		return purprice;
	}
	public void setPurprice(long purprice) {
		this.purprice = purprice;
	}
	public long getProductCategoryid() {
		return productCategoryid;
	}
	public void setProductCategoryid(long productCategoryid) {
		this.productCategoryid = productCategoryid;
	}
	public String getProductCategoryName() {
		return productCategoryName;
	}
	public void setProductCategoryName(String productCategoryName) {
		this.productCategoryName = productCategoryName;
	}
	public long getProductID() {
		return productID;
	}
	public void setProductID(long productID) {
		this.productID = productID;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductValue() {
		return productValue;
	}
	public void setProductValue(String productValue) {
		this.productValue = productValue;
	}
	public long getUnisperpack() {
		return unisperpack;
	}
	public void setUnisperpack(long unisperpack) {
		this.unisperpack = unisperpack;
	}
	public long getClientid() {
		return clientid;
	}
	public void setClientid(long clientid) {
		this.clientid = clientid;
	}
	public long getOrgid() {
		return orgid;
	}
	public void setOrgid(long orgid) {
		this.orgid = orgid;
	}
	public Long getM_attributeset_id() {
		return m_attributeset_id;
	}
	public void setM_attributeset_id(Long m_attributeset_id) {
		this.m_attributeset_id = m_attributeset_id;
	}
	
	public String getThuoctinh() {
		return thuoctinh;
	}
	public void setThuoctinh(String thuoctinh) {
		this.thuoctinh = thuoctinh;
	}
	public long getUomID() {
		return uomID;
	}
	public void setUomID(long uomID) {
		this.uomID = uomID;
	}
	public String getDvt() {
		return dvt;
	}
	public void setDvt(String dvt) {
		this.dvt = dvt;
	}
	
}
