 package com.hdsoft.JptAPI.HDS.model;

import java.util.Date;

public class M_MovementInfomation {
	private long movementID;
	private String documentno;
	private Date movementdate;
	private String docstatus;
	private String docaction;
	private String status;
	private String docstatusconvert;
	private String statusconvert;
	
	public String getDocstatusconvert() {
		return docstatusconvert;
	}
	public void setDocstatusconvert(String docstatusconvert) {
		this.docstatusconvert = docstatusconvert;
	}
	public String getStatusconvert() {
		return statusconvert;
	}
	public void setStatusconvert(String statusconvert) {
		this.statusconvert = statusconvert;
	}
	public long getMovementID() {
		return movementID;
	}
	public void setMovementID(long movementID) {
		this.movementID = movementID;
	}
	public String getDocumentno() {
		return documentno;
	}
	public void setDocumentno(String documentno) {
		this.documentno = documentno;
	}
	public Date getMovementdate() {
		return movementdate;
	}
	public void setMovementdate(Date movementdate) {
		this.movementdate = movementdate;
	}
	public String getDocstatus() {
		return docstatus;
	}
	public void setDocstatus(String docstatus) {
		this.docstatus = docstatus;
	}
	public String getDocaction() {
		return docaction;
	}
	public void setDocaction(String docaction) {
		this.docaction = docaction;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
