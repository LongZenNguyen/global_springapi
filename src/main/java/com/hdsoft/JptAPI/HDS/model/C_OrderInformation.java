 package com.hdsoft.JptAPI.HDS.model;

import java.sql.Timestamp;

public class C_OrderInformation {
	private long c_order_id;
	private String documentno;
	private String dateordered;
	private String c_bpartner_name;
	private Double priceSales;
	public long getC_order_id() {
		return c_order_id;
	} 
	public void setC_order_id(long c_order_id) {
		this.c_order_id = c_order_id;
	}
	public String getDocumentno() {
		return documentno;
	}
	public void setDocumentno(String documentno) {
		this.documentno = documentno;
	}
	public String getDateordered() {
		return dateordered;
	}
	public void setDateordered(String dateordered) {
		this.dateordered = dateordered;
	}
	public String getC_bpartner_name() {
		return c_bpartner_name;
	}
	public void setC_bpartner_name(String c_bpartner_name) {
		this.c_bpartner_name = c_bpartner_name;
	}
	public Double getPriceSales() {
		return priceSales;
	}
	public void setPriceSales(Double priceSales) {
		this.priceSales = priceSales;
	}
	
}
