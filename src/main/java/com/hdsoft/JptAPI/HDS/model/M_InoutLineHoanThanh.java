 package com.hdsoft.JptAPI.HDS.model;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class M_InoutLineHoanThanh {
	private long m_inout_id;
	private String documentno;
	private Timestamp updated;// ngày hoàn thành trong m_inout
	private String productName;
	private Long productID;
	private Long qty;
	private long uomid;
	private String uomName;
	private Long totalprice;
	private Long m_locator_id;
	private String locatorName;
	
	
	public Long getM_locator_id() {
		return m_locator_id;
	}
	public void setM_locator_id(Long m_locator_id) {
		this.m_locator_id = m_locator_id;
	}
	public String getLocatorName() {
		return locatorName;
	}
	public void setLocatorName(String locatorName) {
		this.locatorName = locatorName;
	}
	public long getUomid() {
		return uomid;
	}
	public void setUomid(long uomid) {
		this.uomid = uomid;
	}
	public String getUomName() {
		return uomName;
	}
	public void setUomName(String uomName) {
		this.uomName = uomName;
	}
	public Long getTotalprice() {
		return totalprice;
	}
	public void setTotalprice(Long totalprice) {
		this.totalprice = totalprice;
	}
	public long getM_inout_id() {
		return m_inout_id;
	}
	public void setM_inout_id(long m_inout_id) {
		this.m_inout_id = m_inout_id;
	}
	public String getDocumentno() {
		return documentno;
	}
	public void setDocumentno(String documentno) {
		this.documentno = documentno;
	}
	public Timestamp getUpdated() {
		return updated;
	}
	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Long getProductID() {
		return productID;
	}
	public void setProductID(Long productID) {
		this.productID = productID;
	}
	public Long getQty() {
		return qty;
	}
	public void setQty(Long qty) {
		this.qty = qty;
	}
	
}
