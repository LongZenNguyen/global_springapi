package com.hdsoft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;


@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
public class HDSSpringBoot_GlobalAppAPI_LongZen {
	public static void main(String[] args) {
		SpringApplication.run(HDSSpringBoot_GlobalAppAPI_LongZen.class, args);
		
	}

}